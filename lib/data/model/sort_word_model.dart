class SortWordModel {
  late String question;
  late List<String> splitWords;
  SortWordModel({
    required this.question,
    required this.splitWords,
  });
  SortWordModel.copyWith(SortWordModel o) {
    question = o.question;
    splitWords = o.splitWords;
  }
  
}
