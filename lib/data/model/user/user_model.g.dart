// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      name: json['name'] as String,
      subject_1:
          (json['subject_1'] as List<dynamic>).map((e) => e as String).toList(),
      subject_2:
          (json['subject_2'] as List<dynamic>).map((e) => e as String).toList(),
      subject_3:
          (json['subject_3'] as List<dynamic>).map((e) => e as String).toList(),
      subject_4:
          (json['subject_4'] as List<dynamic>).map((e) => e as String).toList(),
      subject_5:
          (json['subject_5'] as List<dynamic>).map((e) => e as String).toList(),
      sum: json['sum'] as int,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'subject_1': instance.subject_1,
      'subject_2': instance.subject_2,
      'subject_3': instance.subject_3,
      'subject_4': instance.subject_4,
      'subject_5': instance.subject_5,
      'sum': instance.sum,
    };
