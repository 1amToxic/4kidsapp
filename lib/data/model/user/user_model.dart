import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class User {
  String name;
  List<String> subject_1;
  List<String> subject_2;
  List<String> subject_3;
  List<String> subject_4;
  List<String> subject_5;
  int sum;

  User(
      {required this.name,
      required this.subject_1,
      required this.subject_2,
      required this.subject_3,
      required this.subject_4,
      required this.subject_5,
      required this.sum});

  factory User.withInitial(String name) {
    return User(
        name: name,
        subject_1: List.generate(3, (index) => "0"),
        subject_2: List.generate(3, (index) => "0"),
        subject_3: List.generate(3, (index) => "0"),
        subject_4: List.generate(3, (index) => "0"),
        subject_5: List.generate(3, (index) => "0"),
        sum: 0);
  }

  factory User.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return User.fromJson(data);
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() {
    return 'User{name: $name, subject_1: $subject_1, subject_2: $subject_2, subject_3: $subject_3, subject_4: $subject_4, subject_5: $subject_5, sum: $sum}';
  }
}
