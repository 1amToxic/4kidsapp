import 'dart:convert';

class GrammarModel {
  String imageMeaning;
  String imagePath;
  GrammarModel({
    required this.imageMeaning,
    required this.imagePath,
  });

  GrammarModel copyWith({
    String? imageMeaning,
    String? imagePath,
  }) {
    return GrammarModel(
      imageMeaning: imageMeaning ?? this.imageMeaning,
      imagePath: imagePath ?? this.imagePath,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'imageMeaning': imageMeaning,
      'imagePath': imagePath,
    };
  }

  factory GrammarModel.fromMap(Map<String, dynamic> map) {
    return GrammarModel(
      imageMeaning: map['imageMeaning'] ?? '',
      imagePath: map['imagePath'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory GrammarModel.fromJson(String source) => GrammarModel.fromMap(json.decode(source));

  @override
  String toString() => 'GrammarModel(imageMeaning: $imageMeaning, imagePath: $imagePath)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is GrammarModel &&
      other.imageMeaning == imageMeaning &&
      other.imagePath == imagePath;
  }

  @override
  int get hashCode => imageMeaning.hashCode ^ imagePath.hashCode;
}
