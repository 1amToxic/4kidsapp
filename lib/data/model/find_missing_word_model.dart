class FindMissingWordModel {
  late String question;
  late List<String> answers;
  late int rightAnswerIndex;
  FindMissingWordModel(
      {required this.question,
      required this.answers,
      required this.rightAnswerIndex});
	  
  FindMissingWordModel.copyWith(FindMissingWordModel o){
	  question = o.question;
	  answers = o.answers;
	  rightAnswerIndex = o.rightAnswerIndex;
  }
  
}
