import 'dart:math';
import 'package:for_kids_app/app/utils/enum_screen.dart';
import 'package:for_kids_app/data/model/sort_word_model.dart';
import 'package:for_kids_app/data/service/base/base_service.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:core';

class SortWordService extends BaseService {
  late BehaviorSubject<SortWordModel> currentSentence;

  Sink<SortWordModel> get currentSentenceSink => currentSentence.sink;

  Stream<SortWordModel> get currentSentenceStream => currentSentence.stream;

  late final List<SortWordModel> _data;

  int _currentIndex = 0;

  final Map<ScreenEnum, List<SortWordModel>> _testData = {
    ScreenEnum.first: [],
    ScreenEnum.second: [
      SortWordModel(
          question: "Cô giáo âu yếm nhìn các bạn vui đùa.",
          splitWords: ["cô giáo", "âu yếm", "nhìn", "các bạn", "vui đùa"]),
      SortWordModel(
          question: "Chúng em háo hức chờ đón ngày khai trường.",
          splitWords: ["chúng em", "háo hức", "chờ đón", "ngày khai trường."]),
      SortWordModel(
          question: "Cả lớp múa hát sôi nổi.",
          splitWords: ["Cả lớp", "múa hát", "sôi nổi.",  ]),
    ],
    ScreenEnum.third: [
      SortWordModel(
          question: "Con yêu ông bà nhiều lắm.",
          splitWords: ["Con", "yêu", "ông bà", "nhiều lắm."]),
      SortWordModel(
          question: "Mỗi lần em bị ốm, mẹ rất lo lắng cho em.",
          splitWords: [
            "Mỗi lần",
			"em",
            "bị ốm,",
            "mẹ",
            "rất",
			"lo lắng",
            "cho em",
          ]),
      SortWordModel(
          question: "Vào cuối tuần, cả nhà em đều quây quầy bên nhau.",
          splitWords: [
			  "Vào cuối tuần,",
            "cả nhà em",
			"đều",
            "quây quần",
            "bên nhau."
          ]),
      SortWordModel(
          question: "Ngày 28 tháng 6 là ngày Gia đình Việt Nam.",
          splitWords: ["Ngày 28", "tháng 6", "là ngày", "Gia đình", "Việt Nam."])
    ],
    ScreenEnum.fourth: [],
    ScreenEnum.fifth: []
  };

  void getDataByTopic(ScreenEnum topic) {
    _data = _testData[topic]!;
  }

  SortWordService() {
    // load data from server here;

    currentSentence = BehaviorSubject();
  }

  // vi chuan bi truoc 1 cau hoi
  bool hasNextQuestion() => _currentIndex < _data.length;

  void nextQuestion() async {
    currentSentenceSink.add(_data[_currentIndex++]);
    // if (_currentIndex >= _data.length) _currentIndex = 0;
  }
}
