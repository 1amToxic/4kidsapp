import 'package:for_kids_app/data/model/find_missing_word_model.dart';
import 'package:for_kids_app/data/service/base/base_service.dart';
import 'package:get/get.dart';

import '../../../app/utils/enum_screen.dart';

class FindMissingWordService extends BaseService {
  late Rx<FindMissingWordModel> currentQuestion;
  late List<FindMissingWordModel> _data;
  late int _currentIndex;
  FindMissingWordService() {
    _currentIndex = 0;
  }

  void getDataByTopic(ScreenEnum topic) {
    _data = _testData[topic]!;
    currentQuestion = FindMissingWordModel.copyWith(_data[0]).obs;
  }

  final Map<ScreenEnum, List<FindMissingWordModel>> _testData = {
    ScreenEnum.first: [
      FindMissingWordModel(
        question: "Em ở _____",
        answers: ["Trường học", "Chung cư", "Cô giáo", "Lớp 1"],
        rightAnswerIndex: 1,
      ),
    ],
    ScreenEnum.second: [],
    ScreenEnum.third: [
      FindMissingWordModel(
        question: "Ngày 28 tháng 6 là ____.",
        answers: [
          "ngày Gia đình Việt Nam.",
          "ngày Nhà giáo Việt Nam.",
          "ngày khai giảng năm học.",
          "ngày quốc tế thiếu nhi."
        ],
        rightAnswerIndex: 0,
      ),
      FindMissingWordModel(
        question: "Vào cuối tuần, cả nhà em đều ____ bên nhau.",
        answers: ["dịu dàng", "quây quần", "le lói", "êm đềm"],
        rightAnswerIndex: 1,
      ),
      FindMissingWordModel(
        question: "Em luôn cảm thấy thật vui vì gia đình em đều ____ nhau.",
        answers: ["mỉm cười", "yêu thương", "giận dữ", "vui vẻ"],
        rightAnswerIndex: 1,
      ),
      FindMissingWordModel(
        question:
            "Em thấy thật hạnh phúc vì mẹ em rất ___ khi chăm sóc cho em.",
        answers: ["dịu dàng", "mỉm cười", "buồn bã", "giận dữ"],
        rightAnswerIndex: 0,
      ),
      FindMissingWordModel(
        question: "Khi em cảm thấy buồn, anh trai em đều ___ em.",
        answers: ["quây quần", "lạnh lùng", "dỗ dành", "nói chuyện"],
        rightAnswerIndex: 2,
      ),
    ],
    ScreenEnum.fourth: [
      FindMissingWordModel(
        question: "Mỗi ngày, những tia nắng đều ____ khắp muôn nơi.",
        answers: ["nhảy nhót", "tí tách", "mặt trời", "vui vẻ"],
        rightAnswerIndex: 0,
      ),
      FindMissingWordModel(
        question: " Sau cơn mưa, ____ xuất hiện với nhiều màu sắc rực rỡ.",
        answers: ["buổi sáng", "mặt trăng", "cầu vồng", "mặt trời"],
        rightAnswerIndex: 2,
      ),
      FindMissingWordModel(
        question: "Buổi sáng, ông mặt trời _____ khỏi rặng cây.",
        answers: ["rơi tí tách", "lấp ló", "rực rỡ", "nhô lên"],
        rightAnswerIndex: 3,
      ),
      FindMissingWordModel(
        question: "Em thường thấy các ____ đang vẫy chào em.",
        answers: ["tia nắng", "mặt trăng", "ông sao", "cầu vồng"],
        rightAnswerIndex: 2,
      ),
    ],
    ScreenEnum.fifth: [
      FindMissingWordModel(
        question: "Để trở thành một ___ em cần luyện tập vẽ tranh thường xuyên",
        answers: ["họa sĩ", "bác sĩ", "thợ lặn", "kỹ sư"],
        rightAnswerIndex: 0,
      ),
      FindMissingWordModel(
        question: "Những người ___ thật dũng cảm",
        answers: ["dược sĩ", "phi công", "bác sĩ", "lính cứu hỏa"],
        rightAnswerIndex: 3,
      ),
      FindMissingWordModel(
        question: "Công việc của các bác nông dân là ___ và ___.",
        answers: [
          "trồng trọt/bơi lội",
          "trồng trọt/chăn nuôi",
          "xây dựng/chăn nuôi",
          "xây dựng/trồng trọt"
        ],
        rightAnswerIndex: 1,
      ),
    ],
  };

  bool hasNextQuestion() => _currentIndex < _data.length;

  nextQuestion() {
    currentQuestion.value = _data[_currentIndex++];
  }

  void setData(List<FindMissingWordModel> data) {
    _data = List.from(data);
    _currentIndex = 0;
    if (hasNextQuestion()) nextQuestion();
  }
}
