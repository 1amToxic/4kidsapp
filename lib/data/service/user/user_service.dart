import 'package:cloud_firestore/cloud_firestore.dart';

import '../../model/user/user_model.dart';
import '../base/data_result.dart';

class UserService {
  FirebaseFirestore fireStore;

  UserService(this.fireStore);

  Future<DataResult<User>> getUserFromFirebase(String uid) async {
    try {
      DocumentSnapshot documentSnapshot =
          await fireStore.doc("user/$uid").get();
      if (documentSnapshot.exists) {
        final userModel = User.fromDocumentSnapshot(documentSnapshot);
        return Future.value(DataResult.success(userModel));
      }
      return Future.value(DataResult.failure(APIFailure(400, "NoData")));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(404, ex.toString())));
    }
  }

  Future<DataResult> updateUserToFirebase(User user, String uid) async {
    try {
      await fireStore.doc("user/$uid").update(user.toJson());
      return Future.value(DataResult.success("Success"));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(404, ex.toString())));
    }
  }

  Future<DataResult> addUserToFirebase(User user) async {
    try {
      String uid = "";
      await fireStore
          .collection("user")
          .add(user.toJson())
          .then((value) => uid = value.id);
      return Future.value(DataResult.success(uid));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(404, ex.toString())));
    }
  }


  Future<DataResult> updateFieldUser(String key, dynamic value,String uid) async{
    try {
      await fireStore.doc("user/$uid").update({key:value});
      return Future.value(DataResult.success("Success"));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(404, ex.toString())));
    }
  }
}
