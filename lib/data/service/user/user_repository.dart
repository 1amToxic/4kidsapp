import 'dart:async';

import 'package:for_kids_app/data/model/user/user_model.dart';
import 'package:for_kids_app/data/service/base/data_result.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';

abstract class IUserRepository{
  Future<DataResult<User>> getUserFromFirebase(String uid);
  Future<DataResult> updateUserToFirebase(User user, String uid);
  Future<DataResult> addUserToFirebase(User user);
  Future<DataResult> updateFieldUser(String key, dynamic value,String uid);
}

class UserRepository implements IUserRepository{
  final UserService _userService;


  UserRepository(this._userService);

  @override
  Future<DataResult<User>> getUserFromFirebase(String uid) {
    return _userService.getUserFromFirebase(uid);
  }

  @override
  Future<DataResult> updateUserToFirebase(User user, String uid) {
    return _userService.updateUserToFirebase(user, uid);
  }

  @override
  Future<DataResult> addUserToFirebase(User user) {
    return _userService.addUserToFirebase(user);
  }

  @override
  Future<DataResult> updateFieldUser(String key,dynamic value,String uid) {
    return _userService.updateFieldUser(key,value,uid);
  }
}