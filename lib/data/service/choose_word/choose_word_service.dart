import 'dart:math';

import 'package:for_kids_app/app/core/values/app_assets.dart';
import 'package:for_kids_app/data/model/choose_word_model.dart';
import 'package:get/get.dart';

import '../../../app/utils/enum_screen.dart';

class ChooseWordService {
  late final List<ChooseWordModel> _data;
  late RxList<ChooseWordModel> questions;

  final Map<ScreenEnum, List<ChooseWordModel>> _testData = {
    ScreenEnum.first: [
      ChooseWordModel(
          imagePath: AppAssets.ic_building, wordMeaning: "Chung cư"),
      ChooseWordModel(imagePath: AppAssets.ic_house, wordMeaning: "Nhà ngói"),
      ChooseWordModel(
          imagePath: AppAssets.ic_stilt_house, wordMeaning: "Nhà sàn"),
      ChooseWordModel(
          imagePath: AppAssets.ic_school, wordMeaning: "Trường học"),
      ChooseWordModel(imagePath: AppAssets.ic_student, wordMeaning: "Học sinh"),
      ChooseWordModel(
          imagePath: AppAssets.ic_teacher, wordMeaning: "Thầy giáo"),
    ],
    ScreenEnum.second: [],
    ScreenEnum.third: [],
    ScreenEnum.fourth: [],
    ScreenEnum.fifth: [
      ChooseWordModel(imagePath: AppAssets.ic_pilot, wordMeaning: "Phi công"),
      ChooseWordModel(imagePath: AppAssets.ic_artist, wordMeaning: "Họa sĩ"),
      ChooseWordModel(
          imagePath: AppAssets.ic_fireman, wordMeaning: "Lính cứu hỏa"),
      ChooseWordModel(
          imagePath: AppAssets.ic_flight, wordMeaning: "Lái máy bay"),
      ChooseWordModel(imagePath: AppAssets.ic_fishing, wordMeaning: "Đánh cá"),
      ChooseWordModel(imagePath: AppAssets.ic_doctor, wordMeaning: "Bác sĩ"),
    ]
  };

  ChooseWordService() {
    questions = List<ChooseWordModel>.empty().obs;
  }
  void getDataByTopic(ScreenEnum topic) {
    _data = _testData[topic]!;
    shuffleQuestion();
  }

  Future<void> shuffleQuestion() async {
    _data.shuffle();
    print("_data: ${_data.length}");
    questions.value = _data;
    // if (_currentIndex >= _data.length) {
    //   _currentIndex = 0;
    //   _data.shuffle();
    // }
  }
}
