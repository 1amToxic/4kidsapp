import 'dart:async';
import 'package:for_kids_app/data/service/base/base_service.dart';

import '../../../app/utils/enum_screen.dart';

class YourVoiceService extends BaseService {
  late StreamController _wordsStreamController;
  final Map<ScreenEnum, List<String>> _testWords = {
    ScreenEnum.first: [
      "Xin chào, tớ là học sinh lớp 1.",
      "Con chào thầy",
      "Con chào cô",
      "Tớ chào bạn",
    ],
    ScreenEnum.second: [
      "Học sinh xếp hàng chào cờ",
      "Khi thầy giáo giảng bài, cả lớp đều sôi nổi phát biểu",
      "Cô giáo dạy phải gấp sách vở gọn gàng",
      "Em chăm chú nhìn lên bảng",
    ],
    ScreenEnum.third: [],
    ScreenEnum.fourth: [
      "Mưa rơi tí tách trên sân nhà em",
      "Mặt trăng rất tròn vào những ngày rằm",
      "Em thường nhìn thấy bầu trời có rất nhiều vì sao sáng",
      "Mặt trời le lói đằng sau những tòa nhà chung cư cao tầng",
    ],
    ScreenEnum.fifth: [
      "Nam ước mơ sau này trở thành dược sĩ",
      "Bác sĩ ân cần khám bệnh cho mọi người",
    ]
  };

  late List<String> _dataWord;

  int _currentWordIndex = 0;

  YourVoiceService() {
    _wordsStreamController = StreamController();
  }

  Stream get wordsStream => _wordsStreamController.stream;

  Sink get wordsSink => _wordsStreamController.sink;

  void getDataByTopic(ScreenEnum topic) {
    _dataWord = _testWords[topic]!;
  }

  bool hasNextWord() {
    return _currentWordIndex < _dataWord.length;
  }

  nextWord() {
    wordsSink.add(_dataWord[_currentWordIndex++]);
    // if (_currentWordIndex >= _dataWord.length) _currentWordIndex = 0;
  }

  getWordFromRemote() async {
    // TODO: implement get data from remote
    // _dataWord = _testWords;
  }
}
