import 'package:for_kids_app/data/model/grammar_model.dart';
import 'package:for_kids_app/data/service/base/base_service.dart';

class GrammarService {
  final listGrammarTopic1 = [
    GrammarModel(
        imageMeaning: "Xin chào", imagePath: "assets/icons/ic_waving_hand.svg"),
    GrammarModel(
        imageMeaning: "Trường tiểu học",
        imagePath: "assets/icons/ic_primary_school.svg"),
    GrammarModel(
        imageMeaning: "Học sinh", imagePath: "assets/icons/ic_student.svg"),
    GrammarModel(
        imageMeaning: "Cô giáo",
        imagePath: "assets/icons/ic_female_teacher.svg"),
    GrammarModel(
        imageMeaning: "Thầy giáo",
        imagePath: "assets/icons/ic_male_teacher.svg"),
    GrammarModel(
        imageMeaning: "Chung cư", imagePath: "assets/icons/ic_department.svg"),
    GrammarModel(
        imageMeaning: "Nhà ngói", imagePath: "assets/icons/ic_home.svg"),
  ];
  final listGrammarTopic2 = [
    GrammarModel(
        imageMeaning: "Giáo viên", imagePath: "assets/icons/ic_teachers.svg"),
    GrammarModel(
        imageMeaning: "Cô giáo",
        imagePath: "assets/icons/ic_female_teacher.svg"),
    GrammarModel(
        imageMeaning: "Thầy giáo",
        imagePath: "assets/icons/ic_male_teacher.svg"),
    GrammarModel(
        imageMeaning: "Giảng bài", imagePath: "assets/icons/ic_teaching.svg"),
    GrammarModel(
        imageMeaning: "Xếp hàng", imagePath: "assets/icons/ic_line_up.svg"),
    GrammarModel(
        imageMeaning: "Bảng", imagePath: "assets/icons/ic_black_board.svg"),
    GrammarModel(imageMeaning: "Bút", imagePath: "assets/icons/ic_pen.svg"),
    GrammarModel(
        imageMeaning: "Sách vở", imagePath: "assets/icons/ic_books.svg"),
    GrammarModel(
        imageMeaning: "Chào cờ",
        imagePath: "assets/icons/ic_salute_the_flag.svg"),
    GrammarModel(
        imageMeaning: "Đông vui", imagePath: "assets/icons/ic_crowded.svg"),
    GrammarModel(
        imageMeaning: "Sôi nổi", imagePath: "assets/icons/ic_friendly2.svg"),
    GrammarModel(
        imageMeaning: "Gấp sách vở", imagePath: "assets/icons/ic_fold.svg"),
    GrammarModel(
        imageMeaning: "Thân thiện", imagePath: "assets/icons/ic_friendly.svg"),
    GrammarModel(
        imageMeaning: "Hiệu trưởng", imagePath: "assets/icons/ic_headmaster.svg"),
    GrammarModel(
        imageMeaning: "Âu yếm", imagePath: "assets/icons/ic_cuddle.svg"),
    
  ];
  final listGrammarTopic3 = [
    GrammarModel(imageMeaning: "Ông", imagePath: "assets/icons/ic_grampa2.svg"),
    GrammarModel(imageMeaning: "Bà", imagePath: "assets/icons/ic_gramma2.svg"),
    GrammarModel(imageMeaning: "Bố", imagePath: "assets/icons/ic_father2.svg"),
    GrammarModel(imageMeaning: "Mẹ", imagePath: "assets/icons/ic_mom2.svg"),
    GrammarModel(
        imageMeaning: "Mỉm cười", imagePath: "assets/icons/ic_smile.svg"),
    GrammarModel(
        imageMeaning: "Quây quần", imagePath: "assets/icons/ic_gather.svg"),
    GrammarModel(imageMeaning: "Chị", imagePath: "assets/icons/ic_sister2.svg"),
    GrammarModel(imageMeaning: "Anh", imagePath: "assets/icons/ic_brother.svg"),
    GrammarModel(imageMeaning: "Em", imagePath: "assets/icons/ic_baby.svg"),
    GrammarModel(
        imageMeaning: "Lo lắng", imagePath: "assets/icons/ic_worry.svg"),
    GrammarModel(
        imageMeaning: "Yêu thương", imagePath: "assets/icons/ic_love.svg"),
    GrammarModel(
        imageMeaning: "Dỗ dành", imagePath: "assets/icons/ic_comfort.svg"),
    GrammarModel(
        imageMeaning: "Chăm sóc", imagePath: "assets/icons/ic_care.svg"),
  ];
  final listGrammarTopic4 = [
    GrammarModel(
        imageMeaning: "Ông mặt trời", imagePath: "assets/icons/ic_sun.svg"),
    GrammarModel(
        imageMeaning: "Cầu vồng", imagePath: "assets/icons/ic_rainbow.svg"),
    GrammarModel(imageMeaning: "Mưa", imagePath: "assets/icons/ic_rain.svg"),
    GrammarModel(
        imageMeaning: "Mặt trăng", imagePath: "assets/icons/ic_moon.svg"),
    GrammarModel(
        imageMeaning: "Ông sao", imagePath: "assets/icons/ic_stars.svg"),
    GrammarModel(
        imageMeaning: "Bầu trời", imagePath: "assets/icons/ic_sky.svg"),
    GrammarModel(
        imageMeaning: "Giấc mơ", imagePath: "assets/icons/ic_dream.svg"),
    GrammarModel(
        imageMeaning: "Buổi sáng", imagePath: "assets/icons/ic_morning.svg"),
  ];
  final listGrammarTopic5 = [
    GrammarModel(
        imageMeaning: "Dược sĩ", imagePath: "assets/icons/ic_pharmacist.svg"),
    GrammarModel(
        imageMeaning: "Nhà khoa học",
        imagePath: "assets/icons/ic_scientist.svg"),
    GrammarModel(
        imageMeaning: "Phi công", imagePath: "assets/icons/ic_pilot.svg"),
    GrammarModel(
        imageMeaning: "Họa sĩ", imagePath: "assets/icons/ic_artist.svg"),
    GrammarModel(
        imageMeaning: "Bác sĩ", imagePath: "assets/icons/ic_doctor.svg"),
    GrammarModel(
        imageMeaning: "Kĩ sư", imagePath: "assets/icons/ic_engineer.svg"),
    GrammarModel(
        imageMeaning: "Nông dân", imagePath: "assets/icons/ic_farmer.svg"),
    GrammarModel(
        imageMeaning: "Ngư dân", imagePath: "assets/icons/ic_fisherman.svg"),
    GrammarModel(
        imageMeaning: "Thợ lặn", imagePath: "assets/icons/ic_diver.svg"),
    GrammarModel(
        imageMeaning: "Nhà báo", imagePath: "assets/icons/ic_journalist.svg"),
    GrammarModel(
        imageMeaning: "Lính cứu hỏa", imagePath: "assets/icons/ic_fireman.svg"),
    GrammarModel(
        imageMeaning: "Vẽ tranh", imagePath: "assets/icons/ic_drawing.svg"),
    GrammarModel(
        imageMeaning: "Khám bệnh",
        imagePath: "assets/icons/ic_healthcheck.svg"),
    GrammarModel(
        imageMeaning: "Đánh cá", imagePath: "assets/icons/ic_fishing.svg"),
    GrammarModel(imageMeaning: "Bơi", imagePath: "assets/icons/ic_swim.svg"),
    GrammarModel(
        imageMeaning: "Xây dựng", imagePath: "assets/icons/ic_build.svg"),
    GrammarModel(
        imageMeaning: "Lái máy bay", imagePath: "assets/icons/ic_flight.svg"),
    GrammarModel(
        imageMeaning: "Phát minh", imagePath: "assets/icons/ic_invent.svg"),
    GrammarModel(
        imageMeaning: "Thiết kế", imagePath: "assets/icons/ic_design.svg"),
    GrammarModel(
        imageMeaning: "Chăn nuôi", imagePath: "assets/icons/ic_raising.svg"),
    GrammarModel(
        imageMeaning: "Trồng trọt", imagePath: "assets/icons/ic_plant.svg"),
  ];
}
