import 'package:for_kids_app/data/model/grammar_model.dart';
import 'package:for_kids_app/data/service/base/base_service.dart';

import '../../models/flip_card_model.dart';

class FlipThePuzzleService {
  static var listFlipThePuzzleTopic2 = [
    const FlipCardModel('Giảng bài', 'assets/icons/ic_teaching.svg'),
    const FlipCardModel('Bảng', 'assets/icons/ic_black_board.svg'),
    const FlipCardModel('Xếp hàng', 'assets/icons/ic_line_up.svg'),
    const FlipCardModel('Thân thiện', 'assets/icons/ic_friendly.svg'),
    const FlipCardModel('Sách vở', 'assets/icons/ic_books.svg'),
    const FlipCardModel('Chào cờ', 'assets/icons/ic_salute_the_flag.svg'),
  ];
  static var listFlipThePuzzleTopic4 = [
    const FlipCardModel('Cầu vồng', 'assets/icons/ic_rainbow.svg'),
    const FlipCardModel('Tia nắng', 'assets/icons/ic_sun.svg'),
    const FlipCardModel('Mặt trăng', 'assets/icons/ic_moon.svg'),
    const FlipCardModel('Mưa', 'assets/icons/ic_rain.svg'),
    const FlipCardModel('Bầu trời', 'assets/icons/ic_sky.svg'),
    const FlipCardModel('Giấc mơ', 'assets/icons/ic_dream.svg'),
  ];
}
