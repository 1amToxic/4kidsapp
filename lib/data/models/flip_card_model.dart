import 'package:equatable/equatable.dart';

class FlipCardModel extends Equatable {
  const FlipCardModel(this.wordMeaning, this.imagePath);
  final String wordMeaning;
  final String imagePath;

  @override
  List<Object?> get props => [wordMeaning, imagePath];
}

enum CardState { head, tail, correct, incorrect, disappear }
