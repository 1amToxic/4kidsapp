import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'app/core/values/app_theme.dart';
import 'app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]).then(
    (_) => runApp(
      GetMaterialApp(
        debugShowCheckedModeBanner: false,
        getPages: AppPages.routes,
        initialRoute: AppRoutes.splashScreen,
        theme: appTheme,
      ),
    ),
  );
}
