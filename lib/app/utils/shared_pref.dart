import 'package:shared_preferences/shared_preferences.dart';

abstract class ISharedPreferenceApp {
  void setInt(String key, int value);

  Future<int> getInt(String key);

  void setString(String key, String value);

  Future<String> getString(String key);

  Future<List<String>> getListString(String key);

  void setListString(String key, List<String> value);
}

class SharedPreferenceApp implements ISharedPreferenceApp {
  @override
  Future<int> getInt(String key) async {
    final sharePref = await SharedPreferences.getInstance();
    return Future.value(sharePref.getInt(key) ?? -1);
  }

  @override
  void setInt(String key, int value) async {
    final sharePref = await SharedPreferences.getInstance();
    sharePref.setInt(key, value);
  }

  @override
  Future<String> getString(String key) async {
    final sharePref = await SharedPreferences.getInstance();
    return Future.value(sharePref.getString(key) ?? "");
  }

  @override
  void setString(String key, String value) async {
    final sharePref = await SharedPreferences.getInstance();
    sharePref.setString(key, value);
  }

  @override
  Future<List<String>> getListString(String key) async{
    final sharePref = await SharedPreferences.getInstance();
    return Future.value(sharePref.getStringList(key));
  }

  @override
  void setListString(String key, List<String> value) async{
    final sharePref = await SharedPreferences.getInstance();
    sharePref.setStringList(key, value);
  }
}
