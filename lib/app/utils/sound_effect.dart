import 'package:audioplayers/audioplayers.dart';

class SoundEffect {
  static late AudioCache _audioCache;
  static late AudioPlayer _audioPlayer;
  static void playWrongSound() async {
    _audioCache = AudioCache(
      prefix: 'assets/raw/',
      fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
    );
    _audioPlayer = await _audioCache.play("wrong_answer.mp3");
  }
  static void playRightSound() async {
    _audioCache = AudioCache(
      prefix: 'assets/raw/',
      fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
    );
    _audioPlayer = await _audioCache.play("correct_answer.mp3");
  }
  static void playCongratulationSound() async {
    _audioCache = AudioCache(
      prefix: 'assets/raw/',
      fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
    );
    _audioPlayer = await _audioCache.play("congratulation.mp3");
  }
  static void stopSound() async {
    await _audioPlayer.stop();
  }
}
