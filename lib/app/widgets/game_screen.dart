import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:for_kids_app/app/core/values/app_assets.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:get/get.dart';

class GameScreen extends StatelessWidget {
  RxInt? scores;
  bool showLaurel;
  bool showAsk;
  bool showSetting;
  bool showBack;
  Widget? title;
  EdgeInsetsGeometry? paddingContent;
  VoidCallback? onAskClick;
  VoidCallback? onSettingsClick;
  VoidCallback? onBackPress;
  Widget child;

  GameScreen(
      {Key? key,
      this.title,
      this.showLaurel = false,
      this.showAsk = true,
      this.showSetting = true,
      this.showBack = true,
      this.scores,
      this.paddingContent,
      this.onAskClick,
      this.onSettingsClick,
      this.onBackPress,
      required this.child})
      : super(key: key) {
    if (showLaurel && scores == null) {
      scores = 0.obs;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: OnBoardingColor.colorAppBackgroundDark,
      child: Stack(
        children: [
          Column(
            children: [
              SizedBox(
                height: Get.size.height / 50,
              ),
              _HeaderGame(
                title: title,
                showLaurel: showLaurel,
                showSetting: showSetting,
                showAsk: showAsk,
                scores: scores,
                onAskClick: onAskClick,
                onSettingsClick: onSettingsClick,
              ),
              _ContentGame(
                padding: paddingContent,
                child: child,
              ),
            ],
          ),
          showBack
              ? Positioned(
                  left: 24,
                  bottom: 24,
                  child: GestureDetector(
                    onTap: onBackPress ?? Get.back,
                    child: SvgPicture.asset(AppAssets.ic_back),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }

//   void _onBackPress(BuildContext context) {
//     BackDialog(content: Text("Bạn có muốn thoát khỏi màn hình không?"))
//         .show();
//   }
}

class _HeaderGame extends StatelessWidget {
  RxInt? scores;
  bool showLaurel;
  bool showAsk;
  bool showSetting;
  Widget? title;
  VoidCallback? onAskClick;
  VoidCallback? onSettingsClick;
  late Size size;

  _HeaderGame(
      {Key? key,
      this.title,
      this.showLaurel = false,
      this.showAsk = true,
      this.showSetting = true,
      this.scores,
      this.onAskClick,
      this.onSettingsClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Row(
      children: [
        SizedBox(
          width: size.width / 40,
        ),
        showLaurel
            ? raurel()
            : SizedBox(
                width: size.width / 25,
              ),
        Expanded(
          child: title == null ? const SizedBox() : Center(child: title!),
        ),
        const SizedBox(
          width: 5,
        ),
        showAsk
            ? GestureDetector(
                onTap: onAskClick,
                child: SvgPicture.asset(AppAssets.ic_ask),
              )
            : const SizedBox(),
        const SizedBox(
          width: 10,
        ),
        showSetting
            ? GestureDetector(
                onTap: () => _goToSettings(),
                child: SvgPicture.asset(AppAssets.ic_setting),
              )
            : const SizedBox(),
        SizedBox(
          width: size.width / 40,
        ),
      ],
    );
  }

  void _goToSettings() {
    Get.toNamed(AppRoutes.settingsScreen);
  }

  Widget raurel() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        border: Border.all(
          width: 3.0,
          color: BaseDarkColor.colorSystemYellow,
        ),
      ),
      width: 110,
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
           DefaultTextStyle(
              style: const TextStyle(
                  fontSize: 24,
                  color: OnBoardingColor.colorButtonNav,
                  fontWeight: FontWeight.bold),
              child: Obx(() => Text("${scores?.value}"))),
          const SizedBox(
            width: 5,
          ),
          SvgPicture.asset(
            AppAssets.ic_laurel,
          ),
        ],
      ),
    );
  }
}

class _ContentGame extends StatelessWidget {
  EdgeInsetsGeometry? padding;
  Widget child;

  _ContentGame({Key? key, this.padding, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return SingleChildScrollView(
    //   padding: padding,
    //   child: IntrinsicHeight(
    //     child: child,
    //   ),
    // );
    return Expanded(
      child: Padding(
        padding: padding ?? const EdgeInsets.all(8.0),
        child: child,
      ),
    );
  }
}
