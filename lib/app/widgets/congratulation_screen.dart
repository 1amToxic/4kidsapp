import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/sound_effect.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../core/values/app_assets.dart';

class Congratulation extends StatelessWidget {
  const Congratulation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int score = Get.arguments;
    SoundEffect.playCongratulationSound();
    return Scaffold(
      body: Stack(fit: StackFit.expand, children: [
        Positioned(
          left: -20,
          top: -20,
          child:
              Lottie.asset('assets/gif/congrat2.json', width: 200, height: 200),
        ),
        Positioned(
          right: -20,
          top: -20,
          child:
              Lottie.asset('assets/gif/congrat2.json', width: 200, height: 200),
        ),
        Positioned(
          left: 10,
          top: 10,
          child: SvgPicture.asset('assets/icons/ic_leftflags.svg'),
        ),
        Positioned(
          right: 10,
          top: 10,
          child: SvgPicture.asset('assets/icons/ic_rightflags.svg'),
        ),
        Positioned(
          left: 0,
          bottom: 0,
          right: 0,
          top: 0,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  CommonString.congratulation,
                  style: TextStyle(
                    fontSize: 32,
                    color: OnBoardingColor.colorButtonNav,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      score.toString(),
                      style: const TextStyle(
                        fontSize: 32,
                        color: OnBoardingColor.colorButtonNav,
                      ),
                    ),
                    SvgPicture.asset('assets/icons/ic_big_laurel.svg'),
                  ],
                )
              ],
            ),
          ),
        ),
        Positioned(
          child: Lottie.asset('assets/gif/congrat.json'),
        ),
        Positioned(
          left: 10,
          bottom: 10,
          child: GestureDetector(
            child: SvgPicture.asset(AppAssets.ic_back),
            onTap: () => Get.back(),
          ),
        ),
      ]),
    );
  }
}
