import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

import '../core/values/app_colors.dart';

class BackDialog {
  Widget? content;

  BackDialog({this.content});

  void show() {
    Get.defaultDialog(
      title: "",
      backgroundColor: Colors.transparent,
      content: Container(
        padding: const EdgeInsets.all(18.0),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(16)),
        child: content,
      ),
      confirm: GestureDetector(
        onTap: () {
          Get.back();
          Get.back();
        },
        child: Container(
          padding: const EdgeInsets.all(5.0),
          height: Get.height / 10,
          width: Get.width / 7,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: OnBoardingColor.colorAppBackgroundDark,
            border: Border.all(
              width: 3.0,
              color: BaseDarkColor.colorSystemYellow,
            ),
          ),
          child: FittedBox(
            child: Text(
              "Có",
              style: Get.textTheme.headline2!.copyWith(
                color: BaseDarkColor.colorSystemYellow,
              ),
            ),
          ),
        ),
      ),
      cancel: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Container(
          padding: const EdgeInsets.all(5.0),
          height: Get.height / 10,
          width: Get.width / 7,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: BaseDarkColor.colorSystemYellow,
          ),
          child: FittedBox(
            child: Text(
              "Không",
              style: Get.textTheme.headline3!.copyWith(
                color: OnBoardingColor.colorAppBackgroundDark,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
