import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:get/get.dart';

class AskAndSettingsWidget extends StatelessWidget {
  const AskAndSettingsWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: Row(
        children: [
          Flexible(
            flex: 3,
            child: SvgPicture.asset("assets/icons/ic_ask.svg"),
          ),
          const Spacer(),
          Flexible(
            flex: 3,
            child: GestureDetector(onTap: () => _goToSettingScreen(), child: SvgPicture.asset("assets/icons/ic_setting.svg")),
          )
        ],
      ),
    );
  }

  void _goToSettingScreen() {
    Get.toNamed(AppRoutes.settingsScreen);
  }


}