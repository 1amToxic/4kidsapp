import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/data/models/flip_card_model.dart';

class FlipCardPuzzle extends StatelessWidget {
  final Widget onBack;
  final CardState state;
  final FlipCardController? controller;

  const FlipCardPuzzle(
      {required this.onBack,
      required this.controller,
      required this.state,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (state) {
      case CardState.correct:
        return FlipCard(
          flipOnTouch: false,
          controller: controller,
          speed: 500,
          back: Stack(
            alignment: Alignment.center,
            fit: StackFit.passthrough,
            children: [
              SvgPicture.asset('assets/icons/ic_card_correct.svg'),
              Positioned(
                  child: Center(child: onBack),
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0)
            ],
          ),
          front: SvgPicture.asset('assets/icons/ic_card.svg'),
        );
      case CardState.incorrect:
        return FlipCard(
          flipOnTouch: false,
          controller: controller,
          speed: 500,
          back: Stack(
            alignment: Alignment.center,
            fit: StackFit.passthrough,
            children: [
              SvgPicture.asset('assets/icons/ic_card_incorrect.svg'),
              Positioned(
                  child: Center(child: onBack),
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0)
            ],
          ),
          front: SvgPicture.asset('assets/icons/ic_card.svg'),
        );
      case CardState.disappear:
        return Stack(
          alignment: Alignment.center,
          fit: StackFit.passthrough,
          children: [SvgPicture.asset('assets/icons/ic_card_empty.svg')],
        );
      default:
        return FlipCard(
          flipOnTouch: false,
          controller: controller,
          speed: 500,
          back: Stack(
            alignment: Alignment.center,
            fit: StackFit.passthrough,
            children: [
              SvgPicture.asset('assets/icons/ic_card.svg'),
              Positioned(
                  child: Center(child: onBack),
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0)
            ],
          ),
          front: SvgPicture.asset('assets/icons/ic_card.svg'),
        );
    }
  }
}
