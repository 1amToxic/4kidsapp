import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/features/settings/viewmodels/edit_user_name_viewmodel.dart';
import 'package:get/get.dart';

import '../../data/service/user/user_repository.dart';
import '../../data/service/user/user_service.dart';
import '../utils/shared_pref.dart';

class EditUserNameBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => EditUserViewModel(SharedPreferenceApp(),
        UserRepository(UserService(FirebaseFirestore.instance))));
  }

}