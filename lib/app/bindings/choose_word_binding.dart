import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/features/choose_word/viewmodel/choose_word_viewmodel.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/choose_word/choose_word_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../data/service/user/user_service.dart';

class ChooseWordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChooseWordViewModel>(() => ChooseWordViewModel(
          service: ChooseWordService(),
          sharedPreferenceApp: SharedPreferenceApp(),
          userRepository: UserRepository(UserService(FirebaseFirestore.instance)),
        ));
  }
}
