import 'package:get/get.dart';

import '../features/grammar/viewmodels/grammar_viewmodel.dart';

class GrammarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<GrammarViewModel>(() => GrammarViewModel());
  }
}
