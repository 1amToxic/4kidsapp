import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';

import '../features/onboard/viewmodels/onboard_viewmodel.dart';
import 'package:get/get.dart';

class OnBoardBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OnBoardViewModel>(() => OnBoardViewModel(SharedPreferenceApp(),
        UserRepository(UserService(FirebaseFirestore.instance))));
  }
}
