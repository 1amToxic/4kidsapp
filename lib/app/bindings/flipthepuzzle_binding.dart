import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';
import 'package:get/get.dart';

import '../features/flip_the_puzzle/viewmodels/flipthepuzzle_viewmodel.dart';
import '../utils/shared_pref.dart';

class FlipThePuzzleBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FlipThePuzzleViewModel>(() => 
    FlipThePuzzleViewModel(SharedPreferenceApp(), UserRepository(UserService(FirebaseFirestore.instance))),
    );
  }
}
