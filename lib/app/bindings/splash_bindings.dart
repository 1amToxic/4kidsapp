import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/features/splash/view_model/splash_view_model.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';
import 'package:get/get.dart';

class SplashBindings implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => SplashViewModel(SharedPreferenceApp(),UserRepository(UserService(FirebaseFirestore.instance))));
  }
}