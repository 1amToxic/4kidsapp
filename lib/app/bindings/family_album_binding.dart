import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/features/family_album/viewmodel/family_album_viewmodel.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';
import 'package:get/get.dart';

class FamilyAlbumBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FamilyAlbumViewModel>(() => FamilyAlbumViewModel(SharedPreferenceApp(), UserRepository(UserService(FirebaseFirestore.instance))));
  }
}
