import 'package:for_kids_app/app/utils/shared_pref.dart';

import '../features/home/viewmodels/home_viewmodel.dart';
import 'package:get/get.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeViewModel>(() => HomeViewModel(SharedPreferenceApp()));
  }
}
