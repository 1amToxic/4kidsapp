import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/features/sort_word/viewmodel/sort_word_viewmodel.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/sort_word/sort_word_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';
import 'package:get/get.dart';

class SortWordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SortWordViewModel>(() => SortWordViewModel(
        service: SortWordService(),
        sharedPreferenceApp: SharedPreferenceApp(),
        userRepository:
            UserRepository(UserService(FirebaseFirestore.instance))));
  }
}
