import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/find_missing_word/find_missing_word_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';
import 'package:get/get.dart';

class FindMissingWordBinding extends Bindings {
  @override
  void dependencies() {	
    Get.lazyPut<FindMissingWordViewModel>(() => FindMissingWordViewModel(
          service: FindMissingWordService(),
		  sharedPreferenceApp: SharedPreferenceApp(),
		  userRepository: UserRepository(UserService(FirebaseFirestore.instance))
        ));
  }
}
