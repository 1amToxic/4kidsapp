import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:for_kids_app/app/features/your_voice/viewmodel/your_voice_viewmodel.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/user/user_service.dart';
import 'package:for_kids_app/data/service/your_voice/your_voice_service.dart';
import 'package:get/get.dart';

class YourVoiceBinding extends Bindings {
  @override
  void dependencies() {
	//   Get.pu
    Get.lazyPut<YourVoiceViewModel>(() => YourVoiceViewModel(service: YourVoiceService(), sharedPreferenceApp: SharedPreferenceApp(), userRepository: UserRepository(UserService(FirebaseFirestore.instance))));
  }
}
