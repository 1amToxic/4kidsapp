class AppAssets {
  static const String _imagePath = "assets/images/";
  static const String _iconPath = "assets/icons/";

  static const String img_splash = _imagePath + "img_splash.png";

  static const String ic_laurel = _iconPath + "ic_laurel.svg";
  static const String ic_ask = _iconPath + "ic_ask.svg";
  static const String ic_setting = _iconPath + "ic_setting.svg";
  static const String ic_ac_listen = _iconPath + "ic_ac_listen.svg";
  static const String ic_wa_listen = _iconPath + "ic_wa_listen.svg";
  static const String ic_audio_default = _iconPath + "ic_audio_default.svg";
  static const String ic_audio_right = _iconPath + "ic_audio_right.svg";
  static const String ic_audio_wrong = _iconPath + "ic_audio_wrong.svg";
  static const String ic_back = _iconPath + "ic_back.svg";
  static const String ic_pencil = _iconPath + "ic_pencil.svg";
  static const String ic_eraser = _iconPath + "ic_eraser.svg";
  static const String ic_notepad_left = _iconPath + "ic_notepad_left.svg";
  static const String ic_notepad_right = _iconPath + "ic_notepad_right.svg";
  static const String icPronunciation = _iconPath + "ic_pronunciation.svg";
  static const String icPronunciation1 = _iconPath + "ic_pronunciation1.svg";
  static const String icPronunciation2 = _iconPath + "ic_pronunciation2.svg";
  static const String icPronunciation3 = _iconPath + "ic_pronunciation3.svg";
  static const String icPronunciation4 = _iconPath + "ic_pronunciation4.svg";
  static const String icPractice = _iconPath + "ic_practice.svg";
  static const String icPractice1 = _iconPath + "ic_practice1.svg";
  static const String icPractice2 = _iconPath + "ic_practice2.svg";
  static const String icPractice3 = _iconPath + "ic_practice3.svg";
  static const String icPractice4 = _iconPath + "ic_practice4.svg";
  static const String ic_building = _iconPath + "ic_building.svg";
  static const String ic_house = _iconPath + "ic_house.svg";
  static const String ic_school = _iconPath + "ic_school.svg";
  static const String ic_stilt_house = _iconPath + "ic_stilt_house.svg";
  static const String ic_student = _iconPath + "ic_student.svg";
  static const String ic_teacher = _iconPath + "ic_teacher.svg";
  static const String ic_flight = _iconPath + "ic_flight.svg";
  static const String ic_artist = _iconPath + "ic_artist.svg";
  static const String ic_fireman = _iconPath + "ic_fireman.svg";
  static const String ic_pilot = _iconPath + "ic_pilot.svg";
  static const String ic_fishing = _iconPath + "ic_fishing.svg";
  static const String ic_doctor = _iconPath + "ic_doctor.svg";





}

class AppImage{
  static const String imageBridgeIntroduction = "assets/images/bridge_introduction.png";
  static const String imageBridge = "assets/images/bridge.png";



}
