import 'package:flutter/material.dart';

class OnBoardingColor {
  static const Color colorAppBackgroundLight =
      Color.fromARGB(255, 29, 161, 242); //rgba(29, 161, 242, 1)
  static const Color colorAppBackgroundDark =
      Color.fromARGB(255, 49, 50, 111); //background: rgba(49, 50, 111, 1);
  static const Color colorButtonNav =
      Color.fromARGB(255, 255, 201, 60); //background: rgba(255, 201, 60, 1);

}

class GameColor {
  static const Color colorLabelBackground = Color.fromARGB(255, 219, 246, 233);
  static const Color colorWrongAnswerBackground =
      Color.fromARGB(255, 255, 59, 48); //background: rgba(255, 59, 48, 1);
  static const Color colorRightAnswerBackground =
      Color.fromARGB(255, 52, 199, 89); //background: rgba(52, 199, 89, 1);

}

class SettingsColor{
  static const Color colorTextButton = Color.fromARGB(255,49, 50, 111);//rgba(49, 50, 111, 1)
}

class BaseLightColor {
  static const Color colorSystemRed =
      Color.fromARGB(255, 255, 59, 48); //rgba(255, 59, 48, 1)
  static const Color colorSystemOrange =
      Color.fromARGB(255, 255, 149, 0); //rgba(255, 149, 0, 1)
  static const Color colorSystemYellow =
      Color.fromARGB(255, 255, 204, 0); //rgba(255, 204, 0, 1)
  static const Color colorSystemGreen =
      Color.fromARGB(255, 52, 199, 89); //rgba(52, 199, 89, 1)
  static const Color colorSystemTeal =
      Color.fromARGB(255, 90, 200, 250); //rgba(90, 200, 250, 1)
  static const Color colorSystemBlue =
      Color.fromARGB(255, 0, 122, 255); //rgba(0, 122, 255, 1)
  static const Color colorSystemIndigo =
      Color.fromARGB(255, 88, 86, 214); //rgba(88, 86, 214, 1)
  static const Color colorSystemPurple =
      Color.fromARGB(255, 175, 82, 222); //rgba(175, 82, 222, 1)
  static const Color colorSystemPink =
      Color.fromARGB(255, 255, 45, 85); //rgba(255, 45, 85, 1)
  static const Color colorSystemGray1 =
      Color.fromARGB(255, 142, 142, 147); //rgba(142, 142, 147, 1)
  static const Color colorSystemGray2 =
      Color.fromARGB(255, 174, 174, 178); //rgba(174, 174, 178, 1)
  static const Color colorSystemGray3 =
      Color.fromARGB(255, 199, 199, 204); //rgba(199, 199, 204, 1)
  static const Color colorSystemGray4 =
      Color.fromARGB(255, 209, 209, 214); //rgba(209, 209, 214, 1)
  static const Color colorSystemGray5 =
      Color.fromARGB(255, 229, 229, 234); //rgba(229, 229, 234, 1)
  static const Color colorSystemGray6 =
      Color.fromARGB(255, 242, 242, 247); //rgba(242, 242, 247, 1)
}

class BaseDarkColor {
  static const Color colorSystemRed =
      Color.fromARGB(255, 255, 69, 58); //rgba(255, 59, 48, 1)
  static const Color colorSystemOrange =
      Color.fromARGB(255, 255, 159, 10); //rgba(255, 149, 0, 1)
  static const Color colorSystemYellow =
      Color.fromARGB(255, 255, 214, 10); //rgba(255, 204, 0, 1)
  static const Color colorSystemGreen =
      Color.fromARGB(255, 50, 215, 75); //rgba(52, 199, 89, 1)
  static const Color colorSystemTeal =
      Color.fromARGB(255, 100, 210, 255); //rgba(90, 200, 250, 1)
  static const Color colorSystemBlue =
      Color.fromARGB(255, 10, 132, 255); //rgba(0, 122, 255, 1)
  static const Color colorSystemIndigo =
      Color.fromARGB(255, 94, 92, 230); //rgba(88, 86, 214, 1)
  static const Color colorSystemPurple =
      Color.fromARGB(255, 191, 90, 242); //rgba(175, 82, 222, 1)
  static const Color colorSystemPink =
      Color.fromARGB(255, 255, 45, 85); //rgba(255, 45, 85, 1)
  static const Color colorSystemGray1 =
      Color.fromARGB(255, 142, 142, 147); //rgba(142, 142, 147, 1)
  static const Color colorSystemGray2 =
      Color.fromARGB(255, 99, 99, 102); //rgba(174, 174, 178, 1)
  static const Color colorSystemGray3 =
      Color.fromARGB(255, 72, 72, 74); //rgba(199, 199, 204, 1)
  static const Color colorSystemGray4 =
      Color.fromARGB(255, 58, 58, 60); //rgba(209, 209, 214, 1)
  static const Color colorSystemGray5 =
      Color.fromARGB(255, 44, 44, 46); //rgba(229, 229, 234, 1)
  static const Color colorSystemGray6 =
      Color.fromARGB(255, 28, 28, 30); //rgba(242, 242, 247, 1)
}
