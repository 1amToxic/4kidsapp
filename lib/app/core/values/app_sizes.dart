import 'package:flutter/material.dart';

const EdgeInsets defaultPadding = EdgeInsets.symmetric(horizontal: 18.0,vertical: 24.0);
class SettingsScreenSize{
  static  BorderRadiusGeometry buttonBorderRadius = BorderRadius.circular(14.0);
  static double buttonBorderSideWidth = 3.0;
}