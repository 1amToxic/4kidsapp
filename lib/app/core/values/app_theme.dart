import 'package:flutter/material.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';

final ThemeData appTheme = ThemeData(
  primaryColor: OnBoardingColor.colorAppBackgroundDark,
  backgroundColor: OnBoardingColor.colorAppBackgroundDark,
  scaffoldBackgroundColor: OnBoardingColor.colorAppBackgroundDark,
  fontFamily: "Pacifico-Regular",
  textTheme: const TextTheme(
    headline1: TextStyle(fontSize: 48,color: Colors.white),
    headline2: TextStyle(fontSize: 36),
	headline3: TextStyle(fontSize: 24),
    button: TextStyle(fontSize: 21),
    bodyText1: TextStyle(fontSize: 24),
    bodyText2: TextStyle(fontSize: 21),
    subtitle1: TextStyle(fontSize: 30),
    subtitle2: TextStyle(fontSize: 17),
  ),
);
