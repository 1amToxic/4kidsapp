class CommonString {
  static const String textPronunciation = "Từ vựng";
  static const String textPractice = "Luyện tập";
  static const String congratulation =
      'Chúc mừng bạn đã hoàn thành trò chơi với';
}

class OnBoardingString {
  static const String textHeadline = "Tên của em";
  static const String textButtonStart = "Bắt đầu";
  static const String textSnackBarTitle = "Lỗi";
  static const String textSnackBarMessage = "Hãy nhập tên của em vào";
}

class SettingsString {
  static const String textHeadlineSettings = "Cài đặt";
  static const String textButton1 = "Tên đăng nhập";
  static const String textButton2 = "Huy hiệu";
  static const String textButton3 = "Điểm tích lũy";
  static const String textButton4 = "Đăng xuất";
}

class HomeString {
  static const String textFirstSubject1 = "Chủ đề 1";
  static const String textFirstSubject2 = "Chủ đề 2";
  static const String textFirstSubject3 = "Chủ đề 3";
  static const String textFirstSubject4 = "Chủ đề 4";
  static const String textFirstSubject5 = "Chủ đề 5";
}

class IntroductionString {
  static const String textSubjectName = "Chủ đề 1: Giới thiệu bản thân";
  static const String textGreetingLabel1 = "Từ vựng";
  static const String textGreetingLabel2 = "Luyện tập";
}

class LovelySchoolString {
  static const String textSubjectName = "Chủ đề 2: Mái trường mến yêu";
  static const String textGreetingLabel1 = "Từ vựng";
  static const String textGreetingLabel2 = "Luyện tập";
}

class FamilyString {
  static const String textSubjectName = "Chủ đề 3: Gia đình";
}

class SkyInEyesString {
  static const String textSubjectName = "Chủ đề 4: Bầu trời trong mắt em";
}

class JobString {
  static const String textSubjectName = "Chủ đề 5: Nghề nghiệp em yêu";
}

class GameNameString {
  static const String wordMatchImage = "Ghép ảnh với từ tương ứng";
  static const String yourVoice = "Giọng nói của em";
  static const String findMissingWord = "Điền từ còn thiếu";
  static const String flipThePuzzle = "Lật mảnh ghép";
  static const String sortWord = "Sắp xếp từ thành câu";
  static const String familyAlbum = "Ghép ảnh với từ tương ứng ";
}

class FlipThePuzzleString {
  static const String title = "Ghép từ thích hợp vào ô dưới mỗi hình ảnh";
}

class EditNameString{
  static const String title = "Tên đăng nhập";
  static const String cancel = "Hủy";
  static const String save = "Lưu";
}
class RewardString{
  static const String title = "Huy hiệu của bé";
  static const String hardWork = "Chăm chỉ";
  static const String master = "Chuyên gia";
}
class PointString{
  static const String title = "Điểm tích lũy";
}
class LogOutString{
  static const String title = "Bạn có chắc chắn muốn đăng xuất";
  static const String cancel = "Hủy";
  static const String logout = "Đăng xuất";
}

class SharePrefKey{
  static const String userName = "user_name";
  static const String uid = "user_id";
  static const String sum = "sum";

  static const String subject1 = "subject_1";
  static const String subject2 = "subject_2";
  static const String subject3 = "subject_3";
  static const String subject4 = "subject_4";
  static const String subject5 = "subject_5";

}