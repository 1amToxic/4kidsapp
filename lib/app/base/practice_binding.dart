import 'package:for_kids_app/app/features/practice/view_model/practice_view_model.dart';
import 'package:get/get.dart';

class PracticeBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => PracticeViewModel());
  }

}