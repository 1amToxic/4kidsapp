import 'package:flutter/material.dart';
import 'package:for_kids_app/app/features/splash/view_model/splash_view_model.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:get/get.dart';

class SplashScreen extends GetView<SplashViewModel> {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    controller.checkUsernameExist(
      () {
        _goToHome();
      },
      () {
        _goToOnBoardScreen();
      },
    );
    return Scaffold(
      body: Image.asset("assets/images/splash.png"),
    );
  }

  void _goToOnBoardScreen() {
    Get.offNamed(AppRoutes.initialScreen);
  }

  void _goToHome() {
    Get.offNamed(AppRoutes.homeScreen);
  }
}
