import 'dart:developer';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/data/model/user/user_model.dart';
import 'package:for_kids_app/data/service/base/data_result.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';

import '../../../core/values/app_strings.dart';
import '../../../utils/shared_pref.dart';

class SplashViewModel extends BaseViewModel {
  final ISharedPreferenceApp _sharedPreferenceApp;
  final IUserRepository _userRepository;

  SplashViewModel(this._sharedPreferenceApp, this._userRepository);

  void delayBeforeOnBoardScreen(VoidCallback callback) {
    Future.delayed(const Duration(seconds: 3), () {
      callback();
    });
  }

  Future<void> checkUsernameExist(
      VoidCallback onUserExist, VoidCallback onUserNotExist) async {
    String username =
        await _sharedPreferenceApp.getString(SharePrefKey.userName);
    if (username.isNotEmpty) {
      log("It Not empty");
      String uid = await _sharedPreferenceApp.getString(SharePrefKey.uid);
      DataResult result = await _userRepository.getUserFromFirebase(uid);
      if (result.isSuccess) {
        User user = result.data;
        log("SVM30: ${user.toString()}");
        _sharedPreferenceApp.setListString(SharePrefKey.subject1, user.subject_1);
        _sharedPreferenceApp.setListString(SharePrefKey.subject2, user.subject_2);
        _sharedPreferenceApp.setListString(SharePrefKey.subject3, user.subject_3);
        _sharedPreferenceApp.setListString(SharePrefKey.subject4, user.subject_4);
        _sharedPreferenceApp.setListString(SharePrefKey.subject5, user.subject_5);
        _sharedPreferenceApp.setInt(SharePrefKey.sum, user.sum);
      } else {
      }
      delayBeforeOnBoardScreen(() => onUserExist());
    } else {
      delayBeforeOnBoardScreen(() => onUserNotExist());
    }
  }
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _logWhenUserGoToApp();
  }

  void _logWhenUserGoToApp() async{
    log("HERERE");
    await FirebaseAnalytics.instance
        .logAppOpen(callOptions: AnalyticsCallOptions(global: true));
  }
}
