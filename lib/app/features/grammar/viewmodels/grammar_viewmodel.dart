import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/data/model/grammar_model.dart';
import 'package:get/get_rx/get_rx.dart';

import '../../../../data/service/grammar/grammar_service.dart';
import '../../../utils/enum_screen.dart';

class GrammarViewModel extends BaseViewModel {
  var controllerCard = FlipCardController();
  FlutterTts flutterTts = FlutterTts();
  late Rx<String> imagePath;
  late Rx<String> imageMeaning;
  late bool onFront;
  late int position;
  late String topic;
  late List<GrammarModel> grammars;
  void init(ScreenEnum topic, String title) {
    this.topic = title;
    onFront = true;
    position = 0;
    switch (topic) {
      case ScreenEnum.first:
        grammars = GrammarService().listGrammarTopic1;
        break;
      case ScreenEnum.second:
        grammars = GrammarService().listGrammarTopic2;
        break;

      case ScreenEnum.third:
        grammars = GrammarService().listGrammarTopic3;
        break;

      case ScreenEnum.fourth:
        grammars = GrammarService().listGrammarTopic4;
        break;

      case ScreenEnum.fifth:
        grammars = GrammarService().listGrammarTopic5;
        break;
    }
    imageMeaning = grammars[position].imageMeaning.obs;
    imagePath = grammars[position].imagePath.obs;
  }

  void nextQuestion() async {
    if (!onFront) {
      controllerCard.toggleCard();
      await Future.delayed(const Duration(milliseconds: 180));
      print(onFront);
      // flip();
    }
    if (isLast()) return;
    position++;
    imagePath.value = grammars[position].imagePath;
    imageMeaning.value = grammars[position].imageMeaning;
  }

  void previousQuestion() {
    if (!onFront) {
      controllerCard.toggleCard();
      print(onFront);
      // flip();
    }
    if (isFirst()) return;
    position--;
    imagePath.value = grammars[position].imagePath;
    imageMeaning.value = grammars[position].imageMeaning;
  }

  bool isFirst() => position == 0;
  bool isLast() => position == grammars.length - 1;

  void flip() {
    onFront = !onFront;
  }

  void read() async {
    if (!onFront) {
      print(onFront);
      flutterTts.setLanguage('vi-VN');
      await flutterTts.speak(imageMeaning.value);
    }
  }
}
