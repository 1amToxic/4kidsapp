import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/features/grammar/viewmodels/grammar_viewmodel.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../../../widgets/back_dialog.dart';

class GrammarGameScreen extends GetView<GrammarViewModel> {
  const GrammarGameScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.init(Get.arguments[0], Get.arguments[1]);
    return Scaffold(
      backgroundColor: OnBoardingColor.colorAppBackgroundDark,
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: GameScreen(
          title: Text(
            CommonString.textPronunciation,
            style: Get.textTheme.headline2!.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Center(
                  child: IconButton(
                      onPressed: () => controller.previousQuestion(),
                      icon:
                          SvgPicture.asset("assets/icons/ic_back_screen.svg"))),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      controller.topic,
                      style: const TextStyle(
                        color: OnBoardingColor.colorButtonNav,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FlipCard(
                      controller: controller.controllerCard,
                      back: Stack(
                        alignment: Alignment.center,
                        children: [
                          SvgPicture.asset(
                              'assets/icons/ic_card_background_2.svg'),
                          Obx(
                            () => Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  controller.imageMeaning.value,
                                  style: const TextStyle(color: Colors.white),
                                ),
                                IconButton(
                                  onPressed: () => controller.read(),
                                  icon: SvgPicture.asset(
                                      'assets/icons/ic_audio.svg'),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      front: Stack(
                        alignment: Alignment.center,
                        children: [
                          SvgPicture.asset(
                              'assets/icons/ic_card_background.svg'),
                          Obx(
                            () => SvgPicture.asset(controller.imagePath.value),
                          ),
                        ],
                      ),
                      direction: FlipDirection.HORIZONTAL,
                      fill: Fill.fillBack,
                      onFlip: () => {
                        controller.flip(),
                        controller.read(),
                      },
                    ),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
              ),
              Center(
                  child: IconButton(
                      onPressed: () => controller.nextQuestion(),
                      icon:
                          SvgPicture.asset("assets/icons/ic_next_screen.svg"))),
            ],
          ),
        ),
      ),
    );
  }

  onBackPress() {
    BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?"))
        .show();
  }
}
