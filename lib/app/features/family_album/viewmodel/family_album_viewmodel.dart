import 'dart:math';

import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../../utils/enum_screen.dart';

class FamilyAlbumViewModel extends BaseViewModel {
  final ISharedPreferenceApp _sharedPreferenceApp;
  final IUserRepository _userRepository;
  FamilyAlbumViewModel(this._sharedPreferenceApp, this._userRepository);

  late var sum;
  late List<String> listResult;
  late var uid;
  late var subjectKey;
  late var index;
  List<double> rotated = [0, 0, 10, 0, -10, 0];
  var scores = 0.obs;
  List<List<double>> position = [
    [1.3, 0.5],
    [1.3, 3],
    [3.9, 2],
    [1.3, 6.3],
    [1.3, 9.0],
    [3.8, 7],
  ];
  var title = "Album gia đình".obs;
  var acceptedImages = List.generate(6, (_) => false).obs;
  var acceptedAnswers = List.generate(6, (_) => false).obs;
  var listImage = [
    ['assets/icons/ic_father.svg', 'Bố'],
    ['assets/icons/ic_mom.svg', 'Mẹ'],
    ['assets/icons/ic_gramma.svg', 'Bà'],
    ['assets/icons/ic_grampa.svg', 'Ông'],
    ['assets/icons/ic_sister.svg', 'Chị'],
    ['assets/icons/ic_younger.svg', 'Em'],
  ];
  var listAnswer = [
    'Mẹ',
    'Ông',
    'Bà',
    'Em',
    'Chị',
    'Bố',
  ];
  void init(screen, subject) async {
    listAnswer.shuffle();
    listImage.shuffle();
    index = subject;
    subjectKey = "subject_${ScreenEnum.values.indexOf(screen) + 1}";
    sum = await _sharedPreferenceApp.getInt(SharePrefKey.sum);
    uid = await _sharedPreferenceApp.getString(SharePrefKey.uid);
    listResult = await _sharedPreferenceApp.getListString(subjectKey);
  }

  void markCorrect(int i) {
    int index = listAnswer.indexWhere((element) => element == listImage[i][1]);
    acceptedAnswers[index] = true;
    acceptedImages[i] = true;
    scores += 10;
    if (scores.value == 60) {
      savePoints();
      Get.offNamed(AppRoutes.congratulationScreen, arguments: scores.value);
    }
  }

  void savePoints() async {
    if (listResult[index] == '0') {
      print('saved');
      listResult[index] = '1';
      _sharedPreferenceApp.setListString(subjectKey, listResult);
      _sharedPreferenceApp.setInt(SharePrefKey.sum, sum + scores.value);
      await _userRepository.updateFieldUser(subjectKey, listResult, uid);
      await _userRepository.updateFieldUser(
          SharePrefKey.sum, sum + scores.value, uid);
      
    }
  }
}
