import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_theme.dart';
import 'package:for_kids_app/app/features/family_album/viewmodel/family_album_viewmodel.dart';
import 'package:for_kids_app/app/widgets/congratulation_screen.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../../../widgets/back_dialog.dart';

class FamilyAlbumScreen extends GetView<FamilyAlbumViewModel> {
  const FamilyAlbumScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.init(Get.arguments['screen'], Get.arguments['subject']);

    return Scaffold(
      body: GameScreen(
          showLaurel: true,
          title: Obx(
            () => Text(
              controller.title.value,
              style: TextStyle(
                  fontSize: appTheme.textTheme.headline2?.fontSize,
                  color: Colors.white),
            ),
          ),
          scores: controller.scores,
          onBackPress: () => onBackPress(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ListOfText(
                startPostition: 0,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: album(),
              ),
              ListOfText(
                startPostition: 3,
              ),
            ],
          )),
    );
  }

  Widget album() {
    return Center(
      child: Stack(
        fit: StackFit.passthrough,
        children: [
          Image.asset("assets/images/img_family.png"),
          for (var i = 0; i < 6; i++)
            Positioned(
              top: Get.size.height / 10 * controller.position[i][0],
              left: Get.size.height / 10 * controller.position[i][1],
              child: DragTarget(
                builder: (context, candidateData, rejectedData) {
                  return Obx(
                    () => targetImage(
                      i,
                      controller.listImage[i][0],
                      controller.acceptedImages[i]
                          ? controller.listImage[i][1]
                          : '',
                    ),
                  );
                },
                onWillAccept: (data) => data == controller.listImage[i][1],
                onAccept: (data) => controller.markCorrect(i),
              ),
            ),
        ],
      ),
    );
  }

  Widget targetImage(int i, String imagePath, String text) {
    return RotationTransition(
        child: Column(
          children: [
            SvgPicture.asset(imagePath),
            Stack(
              alignment: Alignment.center,
              children: [
                SvgPicture.asset('assets/icons/ic_regtangle_answer.svg'),
                Text(text),
              ],
            ),
          ],
        ),
        turns: AlwaysStoppedAnimation(controller.rotated[i] / 360),
    );
  }

  onBackPress() {
    BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?"))
        .show();
  }
}

class ListOfText extends GetView<FamilyAlbumViewModel> {
  final int startPostition;
  const ListOfText({Key? key, required this.startPostition}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (var i = 0; i < 3; i++)
          Draggable(
            feedback: Material(
              color: Colors.transparent,
              child: Text(controller.listAnswer[startPostition + i],
                  style: appTheme.textTheme.headline3),
            ),
            child: Material(
              color: Colors.transparent,
              child: Obx(
                () => Text(
                  controller.listAnswer[startPostition + i],
                  style: TextStyle(
                      color: controller.acceptedAnswers[startPostition + i]
                          ? Colors.transparent
                          : Colors.white,
                      fontSize: appTheme.textTheme.headline3?.fontSize),
                ),
              ),
            ),
            childWhenDragging: Text(controller.listAnswer[startPostition + i],
                style: TextStyle(
                    color: Colors.transparent,
                    fontSize: appTheme.textTheme.headline3?.fontSize)),
            data: controller.listAnswer[startPostition + i],
          ),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    );
  }
}
