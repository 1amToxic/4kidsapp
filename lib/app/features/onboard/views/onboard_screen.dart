import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../core/values/app_colors.dart';
import '../../../core/values/app_strings.dart';
import '../viewmodels/onboard_viewmodel.dart';
import 'widgets/button_with_icon.dart';
import 'package:get/get.dart';

class OnBoardingScreen extends GetView<OnBoardViewModel> {
  OnBoardingScreen({Key? key}) : super(key: key);
  final FocusNode textFieldFocusNode = FocusNode();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(maxHeight: Get.height),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                const Spacer(),
                Expanded(
                  flex: 2,
                  child: Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      onPressed: () {},
                      icon: SvgPicture.asset("assets/icons/ic_chat.svg"),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    OnBoardingString.textHeadline,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: SizedBox(
                    width: 400,
                    child: TextField(
                      focusNode: textFieldFocusNode,
                      onSubmitted: (value) {
                        controller.updateUsername(value);
                      },
                      style: Theme.of(context)
                          .textTheme
                          .headline2!
                          .copyWith(color: BaseDarkColor.colorSystemIndigo),
                      decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(24)),
                          borderSide: BorderSide(
                              width: 2, color: BaseDarkColor.colorSystemYellow),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(24)),
                          borderSide: BorderSide(
                              width: 5, color: BaseDarkColor.colorSystemYellow),
                        ),
                      ),
                      cursorColor: BaseDarkColor.colorSystemIndigo,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                const Spacer(),
                Expanded(flex: 2,child: ButtonWithIconWidget(onButtonClick: checkValidUsername)),
                const Spacer()
              ],
            ),
          ),
        ),
      ),
    );
  }

  void checkValidUsername() {
    if (controller.checkUsername()) {
      controller.saveUsernameToDb(() => _goToHome());

    } else {
      Get.snackbar(OnBoardingString.textSnackBarTitle,
          OnBoardingString.textSnackBarMessage,colorText: Colors.white);
      textFieldFocusNode.requestFocus();
    }
  }
  void _goToHome(){
    Get.offNamed("/home");
  }
}
