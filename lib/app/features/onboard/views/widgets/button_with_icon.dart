import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/values/app_colors.dart';
import '../../../../core/values/app_strings.dart';

class ButtonWithIconWidget extends StatelessWidget {
  const ButtonWithIconWidget({
    required this.onButtonClick,
    Key? key,
  }) : super(key: key);
  final VoidCallback onButtonClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onButtonClick,
      child: Container(
        width: 137,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: BaseDarkColor.colorSystemYellow,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  OnBoardingString.textButtonStart,
                  style: Theme.of(context)
                      .textTheme
                      .button!
                      .copyWith(color: SettingsColor.colorTextButton),
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                child: SvgPicture.asset("assets/icons/ic_start.svg",color: SettingsColor.colorTextButton,),
              )
            ],
          ),
        ),
      ),
    );
  }
}
