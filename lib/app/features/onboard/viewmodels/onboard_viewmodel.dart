import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/model/user/user_model.dart';
import 'package:for_kids_app/data/service/base/data_result.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../base/base_viewmodel.dart';

class OnBoardViewModel extends BaseViewModel {
  final textUsername = "".obs;
  final ISharedPreferenceApp _sharedPreferenceApp;
  final IUserRepository _userRepository;

  OnBoardViewModel(this._sharedPreferenceApp, this._userRepository);

  void updateUsername(String newText) {
    textUsername.value = newText;
  }

  bool checkUsername() {
    if (textUsername.value.trim().isEmpty) {
      return false;
    }
    return true;
  }

  void saveUsernameToDb(VoidCallback goToHome) async {
    _sharedPreferenceApp.setString(SharePrefKey.userName, textUsername.value);
    User userDefault = User.withInitial(textUsername.value);
    DataResult result = await _userRepository
        .addUserToFirebase(userDefault);
    if(result.isSuccess){
      log("OVM37: ${userDefault.subject_1}");
      _sharedPreferenceApp.setListString(SharePrefKey.subject1, userDefault.subject_1);
      _sharedPreferenceApp.setListString(SharePrefKey.subject2, userDefault.subject_2);
      _sharedPreferenceApp.setListString(SharePrefKey.subject3, userDefault.subject_3);
      _sharedPreferenceApp.setListString(SharePrefKey.subject4, userDefault.subject_4);
      _sharedPreferenceApp.setListString(SharePrefKey.subject5, userDefault.subject_5);
      _sharedPreferenceApp.setInt(SharePrefKey.sum, userDefault.sum);
      log("OBM36: SAVE Data to FB Success");
      String uid = result.data;
      _sharedPreferenceApp.setString(SharePrefKey.uid, uid);
      goToHome();
    }else{
      log("OBM38: SAVE Data to FB Fail ${(result.error as APIFailure).errorResponse}");
    }
  }
}
