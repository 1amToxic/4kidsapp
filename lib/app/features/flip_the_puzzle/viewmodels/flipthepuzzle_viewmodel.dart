import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/enum_screen.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/app/utils/sound_effect.dart';
import 'package:for_kids_app/data/models/flip_card_model.dart';
import 'package:for_kids_app/data/service/base/data_result.dart';
import 'package:for_kids_app/data/service/flip_the_puzzle/ftp_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../../routes/app_routes.dart';

class FlipThePuzzleViewModel extends BaseViewModel {
  final ISharedPreferenceApp _sharedPreferenceApp;
  final IUserRepository _userRepository;
  FlipThePuzzleViewModel(this._sharedPreferenceApp, this._userRepository);
  late final List<FlipCardModel> listCard;
  var score = 0.obs;
  late var sum;
  late List<String> listResult;
  late var uid;
  late var subjectKey;
  late var index;
  var listAnswer = List<CardState>.generate(12, (_) => CardState.tail).obs;
  var listQuestion = List<int>.generate(12, (i) => i);
  var listCardsOnFront = [
    [-1, -1],
    [-1, -1]
  ];
  //shuffle
  void init(screen, subject) async {
    listCard = (screen == ScreenEnum.second)
        ? FlipThePuzzleService.listFlipThePuzzleTopic2
        : FlipThePuzzleService.listFlipThePuzzleTopic4;
    listQuestion.shuffle();
    index = subject;
    subjectKey = "subject_${ScreenEnum.values.indexOf(screen) + 1}";
    sum = await _sharedPreferenceApp.getInt(SharePrefKey.sum);
    uid = await _sharedPreferenceApp.getString(SharePrefKey.uid);
    listResult = await _sharedPreferenceApp.getListString(subjectKey);
  }

  var cardController = List.generate(12, (_) => FlipCardController());
  void flip(int i, int j) {
    if (listAnswer[j] == CardState.head) {
      if (listCardsOnFront[0][0] == i) {
        listCardsOnFront[0][0] = -1;
      } else {
        listCardsOnFront[1][0] = -1;
      }
      listAnswer[j] = CardState.tail;
    } else {
      listAnswer[j] = CardState.head;
      if (listCardsOnFront[0][0] == -1) {
        listCardsOnFront[0] = [i, j];
      } else {
        listCardsOnFront[1] = [i, j];
      }
    }
    cardController[i].toggleCard();
    print(listCardsOnFront);
  }

  void correct(int i, int j) {
    listAnswer[i] = CardState.correct;
    listAnswer[j] = CardState.correct;
  }

  void disappear(int i, int j) {
    listAnswer[i] = CardState.disappear;
    listAnswer[j] = CardState.disappear;
  }

  void incorrect(int i, int j) {
    listAnswer[i] = CardState.incorrect;
    listAnswer[j] = CardState.incorrect;
  }

  bool canFlip() =>
      (listCardsOnFront[0][0] == -1 || listCardsOnFront[1][0] == -1);
  bool needToCheck() =>
      (listCardsOnFront[0][0] != -1 && listCardsOnFront[1][0] != -1);
  Future<void> check() async {
    var ans1 = listCardsOnFront[0], ans2 = listCardsOnFront[1];
    if (ans1[1] % 6 == ans2[1] % 6) {
      correct(ans1[1], ans2[1]);
      listCardsOnFront[0][0] = -1;
      listCardsOnFront[1][0] = -1;
      SoundEffect.playRightSound();
      await Future.delayed(const Duration(seconds: 1));
      score += 10;
      if (score.value == 60) {
        savePoints();
        Get.offNamed(AppRoutes.congratulationScreen, arguments: score.value);
      }
      disappear(ans1[1], ans2[1]);
    } else {
      incorrect(ans1[1], ans2[1]);
      SoundEffect.playWrongSound();
      await Future.delayed(const Duration(seconds: 1));

      if (listAnswer[ans1[1]] != CardState.correct &&
          listAnswer[ans1[1]] != CardState.tail) {
        listAnswer[ans1[1]] = CardState.tail;
        cardController[ans1[0]].toggleCard();
        listCardsOnFront[0][0] = -1;
      }

      if (listAnswer[ans2[1]] != CardState.correct &&
          listAnswer[ans2[1]] != CardState.tail) {
        listAnswer[ans2[1]] = CardState.tail;
        cardController[ans2[0]].toggleCard();
        listCardsOnFront[1][0] = -1;
      }
    }
  }

  void savePoints() async {
    if (listResult[index] == '0') {
      listResult[index] = '1';
      _sharedPreferenceApp.setListString(subjectKey, listResult);
      _sharedPreferenceApp.setInt(SharePrefKey.sum, sum + score.value);
      await _userRepository.updateFieldUser(subjectKey, listResult, uid);
      await _userRepository.updateFieldUser(
          SharePrefKey.sum, sum + score.value, uid);
    }
  }
}
