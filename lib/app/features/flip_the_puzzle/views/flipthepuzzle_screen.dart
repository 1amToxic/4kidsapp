import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/features/flip_the_puzzle/viewmodels/flipthepuzzle_viewmodel.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/app/widgets/back_dialog.dart';
import 'package:for_kids_app/app/widgets/congratulation_screen.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../../core/values/app_colors.dart';
import '../../../widgets/flip_card_puzzle.dart';

class FlipThePuzzleScreen extends GetView<FlipThePuzzleViewModel> {
  const FlipThePuzzleScreen({Key? key}) : super(key: key);

  void doWhenClicked(int i, int j) async {
    if (!controller.canFlip()) return;
    controller.flip(i, j);
    if (controller.needToCheck()) {
      await Future.delayed(const Duration(seconds: 1));
      await controller.check();
    }
  }

  @override
  Widget build(BuildContext context) {
    controller.init(Get.arguments['screen'], Get.arguments['subject']);
    return Scaffold(
      backgroundColor: OnBoardingColor.colorAppBackgroundDark,
      body: Center(
        child: GameScreen(
          showLaurel: true,
          scores: controller.score,
          paddingContent: const EdgeInsets.only(
              top: 10.0, left: 30.0, right: 30.0, bottom: 10.0),
          title: const Text(
            FlipThePuzzleString.title,
            style: TextStyle(
                fontSize: 24.0, color: GameColor.colorLabelBackground),
          ),
          onBackPress: () => onBackPress(),
          child: Center(
            child: Table(
              children: [
                TableRow(
                  children: [
                    for (var i = 0; i < 4; i++) cell(i),
                  ],
                ),
                TableRow(
                  children: [
                    for (var i = 4; i < 8; i++)
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: cell(i),
                      ),
                  ],
                ),
                TableRow(
                  children: [
                    for (var i = 8; i < 12; i++)
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: cell(i),
                      ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget cell(int i) {
    return Obx(
      () => GestureDetector(
        onTap: () => doWhenClicked(i, controller.listQuestion[i]),
        child: FlipCardPuzzle(
          onBack: (controller.listQuestion[i] < 6)
              ? Text(
                  controller.listCard[controller.listQuestion[i]].wordMeaning)
              : SvgPicture.asset(controller
                  .listCard[controller.listQuestion[i] % 6].imagePath),
          controller: controller.cardController[i],
          state: controller.listAnswer[controller.listQuestion[i]],
        ),
      ),
    );
  }

  onBackPress() {
    BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?"))
        .show();
  }
}
