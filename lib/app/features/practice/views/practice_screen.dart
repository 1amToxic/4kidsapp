import 'package:flutter/material.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/features/practice/view_model/practice_view_model.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:for_kids_app/app/utils/enum_screen.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../../../core/values/app_colors.dart';
import '../../../core/values/app_sizes.dart';

class PracticeScreen extends GetView<PracticeViewModel> {
  late ScreenEnum screenEnum;

  PracticeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    screenEnum = Get.arguments;
    List<String> listTitle = controller.getListTitle(screenEnum);
    return Material(
      child: GameScreen(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildBorderButton(context, listTitle[0], 0),
            _buildBorderButton(context, listTitle[1], 1),
            _buildBorderButton(context, listTitle[2], 2),
          ],
        ),
      ),
    );
  }

  Widget _buildBorderButton(BuildContext context, String title, int index) {
    return OutlinedButton(
      onPressed: () {
        switch (title) {
          case GameNameString.familyAlbum:
            {
              _goToFamilyAlbum(index);
              break;
            }
          case GameNameString.findMissingWord:
            {
              _goToFindMissingWord(index);
              break;
            }
          case GameNameString.yourVoice:
            {
              _goToYourVoice(index);
              break;
            }
          case GameNameString.flipThePuzzle:
            {
              _goToFlipPuzzle(index);
              break;
            }
          case GameNameString.sortWord:
            {
              _goToSortWord(index);
              break;
            }
          case GameNameString.wordMatchImage:
            {
              _goToWordMatchImage(index);
              break;
            }
        }
      },
      child: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: SettingsColor.colorTextButton),
      ),
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: SettingsScreenSize.buttonBorderRadius,
          ),
        ),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
        side: MaterialStateProperty.all(
          BorderSide(
            color: BaseLightColor.colorSystemYellow,
            width: SettingsScreenSize.buttonBorderSideWidth,
            style: BorderStyle.solid,
          ),
        ),
      ),
    );
  }

  void _goToYourVoice(int index) {
    Get.toNamed(AppRoutes.yourVoiceScreen,
        arguments: <String, dynamic>{'screen': screenEnum, 'subject': index});
  }

  void _goToSortWord(int index) {
    Get.toNamed(AppRoutes.sortWordScreen,
        arguments: <String, dynamic>{'screen': screenEnum, 'subject': index});
  }

  void _goToFamilyAlbum(int index) {
    Get.toNamed(AppRoutes.familyAlbumScreen,
        arguments: <String, dynamic>{'screen': screenEnum, 'subject': index});
  }

  void _goToFlipPuzzle(int index) {
    Get.toNamed(AppRoutes.flipThePuzzleScreen,
        arguments: <String, dynamic>{'screen': screenEnum, 'subject': index});
  }

  void _goToFindMissingWord(int index) {
    Get.toNamed(AppRoutes.findMissingWordScreen,
        arguments: <String, dynamic>{'screen': screenEnum, 'subject': index});
  }

  void _goToWordMatchImage(int index) {
    Get.toNamed(AppRoutes.chooseWordScreen,
        arguments: <String, dynamic>{'screen': screenEnum, 'subject': index});
  }
}
