import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/enum_screen.dart';

class PracticeViewModel extends BaseViewModel {
  final listScreenButtonTitle = [
    [
      GameNameString.wordMatchImage,
      GameNameString.yourVoice,
      GameNameString.findMissingWord
    ],
    [
      GameNameString.flipThePuzzle,
      GameNameString.yourVoice,
      GameNameString.sortWord
    ],
    [
      GameNameString.familyAlbum,
      GameNameString.findMissingWord,
      GameNameString.sortWord
    ],
    [
      GameNameString.flipThePuzzle,
      GameNameString.findMissingWord,
      GameNameString.yourVoice
    ],
    [
      GameNameString.wordMatchImage,
      GameNameString.yourVoice,
      GameNameString.findMissingWord
    ],
  ];

  List<String> getListTitle(ScreenEnum screenEnum) {
    switch (screenEnum) {
      case ScreenEnum.first:
        return listScreenButtonTitle[0];
      case ScreenEnum.second:
        return listScreenButtonTitle[1];
      case ScreenEnum.third:
        return listScreenButtonTitle[2];
      case ScreenEnum.fourth:
        return listScreenButtonTitle[3];
      case ScreenEnum.fifth:
        return listScreenButtonTitle[4];
    }
  }
}
