import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_assets.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/features/sort_word/viewmodel/sort_word_viewmodel.dart';
import 'package:for_kids_app/app/features/sort_word/viewmodel/word_state.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';

import '../../../widgets/back_dialog.dart';

class SortWordScreen extends GetWidget<SortWordViewModel> {
  late SortWordViewModel _viewmodel;
  SortWordScreen({Key? key}) : super(key: key) {
    _viewmodel = Get.find<SortWordViewModel>();
    _viewmodel.init();
  }

  @override
  Widget build(BuildContext context) {
    final horizontalPadding = Get.size.width / 10;
    return Scaffold(
      body: GameScreen(
        showLaurel: true,
        scores: _viewmodel.scores,
        paddingContent:
            EdgeInsets.fromLTRB(horizontalPadding, 0, horizontalPadding, 10),
        onAskClick: onAskPress,
        onSettingsClick: onSettingPress,
        onBackPress: onBackPress,
        child: _sortWordScreen(),
      ),
    );
  }

  Widget _sortWordScreen() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 1,
          child: Obx(
            () => Visibility(
              maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              visible: _viewmodel.showHint.value,
              child: FittedBox(
				child: Text(
				  _viewmodel.hint.value,
				  style: Get.textTheme.bodyLarge!.copyWith(color: Colors.white),
				),
			  ),
            ),
          ),
        ),
        // SizedBox(
        //   height: Get.size.height / 30,
        // ),
        Expanded(
          flex: 4,
          child: FittedBox(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                SvgPicture.asset(AppAssets.ic_pencil),
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(18),
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.vertical(
                            bottom: Radius.circular(25),
                          ),
                        ),
                        width: Get.size.width / 1.5,
                        height: Get.size.height / 3,
                        child: Stack(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              child: Column(children: [
                                SizedBox(
                                  height: Get.height / 24 +
                                      19 * Get.size.height / 200,
                                ),
                                const Divider(
                                  thickness: 1,
                                  color: Colors.grey,
                                ),
                                SizedBox(
                                  height: Get.height / 24 +
                                      9 * Get.size.height / 200,
                                ),
                                const Divider(
                                  thickness: 1,
                                  color: Colors.grey,
                                ),
                              ]),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: Get.size.height / 20,
                                  ),
                                  Obx(
                                    () => Wrap(
                                      spacing: Get.size.width / 80,
                                      runSpacing: Get.size.height / 50,
                                      children: _viewmodel.chooseListWords
                                          .map((element) =>
                                              _buildItemChooseWord(element))
                                          .toList(),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 0,
                      top: 0,
                      child: SvgPicture.asset(AppAssets.ic_notepad_left),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: SvgPicture.asset(AppAssets.ic_notepad_right),
                    ),
                  ],
                ),
                SvgPicture.asset(AppAssets.ic_eraser),
              ],
            ),
          ),
        ),
        // SizedBox(
        //   height: Get.size.height / 25,
        // ),
        Expanded(
          flex: 2,
          child: Obx(
            () => Wrap(
              alignment: WrapAlignment.center,
              spacing: Get.size.width / 30,
              runSpacing: Get.size.height / 70,
              children: _buildListQuestion(_viewmodel.questionListWords),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Widget _buildItemChooseWord(WordState wordState) {
    return Container(
      decoration: BoxDecoration(
        color: wordState.answerState?.color,
        borderRadius: BorderRadius.circular(10),
      ),
      height: Get.height / 9,
      child: Padding(
        padding: EdgeInsets.all(Get.height / 50),
        child: FittedBox(
          child: Text(
            wordState.content,
            style: Get.textTheme.bodyMedium,
          ),
        ),
      ),
    );
  }

  Widget _buildItemQuestionWord(Rx<WordState> wordState, int index) {
    return Obx(
      () => Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: wordState.value.visiable,
        child: GestureDetector(
          onTap: () => _viewmodel.onChooseWord(wordState.value, index),
          child: Container(
            decoration: BoxDecoration(
              color: wordState.value.answerState?.color,
              borderRadius: BorderRadius.circular(10),
            ),
			height: Get.height / 9,
            child: Padding(
              padding: EdgeInsets.all(Get.height / 50),
              child: FittedBox(
				child: Text(
				  wordState.value.content,
				  style: Get.textTheme.bodyMedium,
				),
			  ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildListQuestion(RxList<Rx<WordState>> list) {
    List<Widget> res = List.empty(growable: true);
    for (int i = 0; i < list.length; i++) {
      res.add(_buildItemQuestionWord(list[i], i));
    }
    return res;
  }

  onBackPress() {
	  BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?")).show();
  }

  onSettingPress() {}

  onAskPress() => _viewmodel.onAskPress();
}
