import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:for_kids_app/app/features/sort_word/viewmodel/word_state.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/app/utils/sound_effect.dart';
import 'package:for_kids_app/data/model/sort_word_model.dart';
import 'package:for_kids_app/data/service/sort_word/sort_word_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../../core/values/app_strings.dart';
import '../../../routes/app_routes.dart';
import '../../../utils/enum_screen.dart';

class SortWordViewModel extends BaseViewModel {
  SortWordService service;
  IUserRepository userRepository;
  ISharedPreferenceApp sharedPreferenceApp;
  final scores = 0.obs;
  final showHint = true.obs;
  final hint = "".obs;
  final checkAnswer = 0.obs;

  List<String> listSubject = List.empty();

  String subjectKey = "";

  int currentSubject = 0, sum = 0;

  String uid = "";

  final RxList<WordState> chooseListWords =
      List<WordState>.empty(growable: true).obs;

  final RxList<Rx<WordState>> questionListWords =
      List<Rx<WordState>>.empty(growable: true).obs;

  SortWordViewModel(
      {required this.service,
      required this.sharedPreferenceApp,
      required this.userRepository}) {
    var argument = Get.arguments;
    ScreenEnum topic = argument["screen"];

    service.getDataByTopic(topic);
    service.currentSentenceStream.listen((event) {
      _setUpQuestion(event);
    });

    checkAnswer.listen((value) {
      if (value != 0 && value == questionListWords.length) {
        _onCheckAnwer();
      }
    });
    currentSubject = argument["subject"];
    getDataFromSharePref(topic);
  }

  void getDataFromSharePref(ScreenEnum screenEnum) async {
    subjectKey = "subject_${ScreenEnum.values.indexOf(screenEnum) + 1}";
    listSubject = await sharedPreferenceApp.getListString(subjectKey);
    uid = await sharedPreferenceApp.getString(SharePrefKey.uid);
    sum = await sharedPreferenceApp.getInt(SharePrefKey.sum);
  }

  void init() {
    if (service.hasNextQuestion()) service.nextQuestion();
  }

  void _setUpQuestion(SortWordModel sortWordModel) async {
    print("on _setUpQuestion");
    showHint.value = true;
    hint.value = sortWordModel.question;
    var listWord = sortWordModel.splitWords;
    List<Rx<WordState>> listWordState =
        List<Rx<WordState>>.empty(growable: true);
    for (int i = 0; i < listWord.length; i++) {
      listWordState.add(WordState(content: listWord[i], resultIndex: i).obs);
    }
    listWordState.shuffle();
    questionListWords.value = listWordState;
    Future.delayed(const Duration(seconds: 5), () {
      showHint.value = false;
    });
  }

  void onChooseWord(WordState wordState, int index) async {
    var tmp = List<WordState>.from(chooseListWords);
    tmp.add(wordState);
    chooseListWords.value = tmp;
    print("chooseListWords: ${tmp.length}");
    questionListWords[index].value = wordState.copyWith(visiable: false);
    checkAnswer.value++;
  }

  void _onCheckAnwer() async {
    print("onCheckAnwer");
    var tmp = List<WordState>.from(chooseListWords);
    int rightAnswer = 0;
    for (int i = 0; i < tmp.length; i++) {
      if (tmp[i].resultIndex == i) {
        tmp[i].answerState = RightAnswerAnswerState();
        rightAnswer++;
      } else {
        tmp[i].answerState = WrongAnswerAnswerState();
      }
    }
    chooseListWords.value = tmp;
    if (rightAnswer == tmp.length) {
      scores.value += 10;
      SoundEffect.playRightSound();

      nextQuestion();
    } else {
      SoundEffect.playWrongSound();
      await Future.delayed(
        const Duration(seconds: 2),
      );
      resetQuestion();
    }
  }

  void resetQuestion() async {
    chooseListWords.value = List.empty(growable: true);
    checkAnswer.value = 0;
    var tmp = questionListWords;
    for (int i = 0; i < tmp.length; i++) {
      questionListWords[i].value = tmp[i].value.copyWith(visiable: true);
    }
  }

  void nextQuestion() async {
    if (service.hasNextQuestion()) {
      await Future.delayed(
        const Duration(seconds: 3),
      );
      chooseListWords.value = List.empty(growable: true);
      checkAnswer.value = 0;
      service.nextQuestion();
    } else {
      if (listSubject[currentSubject] == "0") {
        listSubject[currentSubject] = "1";
        sum += scores.value;
        sharedPreferenceApp.setListString(subjectKey, listSubject);
        sharedPreferenceApp.setInt(SharePrefKey.sum, sum);
        userRepository.updateFieldUser(subjectKey, listSubject, uid);
        await userRepository.updateFieldUser(SharePrefKey.sum, sum, uid);
      }
      Get.offNamed(AppRoutes.congratulationScreen, arguments: scores.value);
    }
  }

  void onAskPress() {
    showHint.value = !showHint.value;
  }
}
