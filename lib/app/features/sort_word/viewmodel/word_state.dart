import 'package:flutter/material.dart';
import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:get/state_manager.dart';

class WordState {
  late String content;
  late AnswerState? answerState;
  int resultIndex = 0;
  int questionIndex = 0;
  bool visiable;

  WordState(
      {required this.content,
      required this.resultIndex,
      this.visiable = true}) {
    answerState = InitialAnswerState();
  }
  WordState.withData({
    required this.content,
    required this.resultIndex,
    required this.questionIndex,
    required this.answerState,
    this.visiable = true,
  });
  
  WordState copyWith(
      {String? content,
      int? resultIndex,
      int? questionIndex,
      AnswerState? answerState,
      bool? visiable}) {
    return WordState.withData(
      content: content ?? this.content,
      resultIndex: resultIndex ?? this.resultIndex,
      questionIndex: questionIndex ?? this.questionIndex,
      answerState: answerState ?? this.answerState,
	  visiable: visiable ?? this.visiable
    );
  }
}
