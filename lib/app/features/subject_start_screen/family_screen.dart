import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/enum_screen.dart';
import 'package:get/get.dart';

import '../../core/values/app_assets.dart';
import '../../core/values/app_colors.dart';
import '../../routes/app_routes.dart';
import '../../widgets/game_screen.dart';

class FamilyScreen extends StatelessWidget {
  const FamilyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: GameScreen(
          title: Text(
            FamilyString.textSubjectName,
            style: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(color: BaseDarkColor.colorSystemYellow),
          ),
          onBackPress: () => _goToBackScreen(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () => _goToPronunciation(),
                child: SvgPicture.asset(AppAssets.icPronunciation2),
              ),
              GestureDetector(
                onTap: () => _goToPractice(),
                child: SvgPicture.asset(AppAssets.icPractice2),
              )
            ],
          )),
    );
  }

  void _goToBackScreen() {
    Get.offNamed(AppRoutes.homeScreen);
  }

  void _goToPronunciation() {
    Get.toNamed(AppRoutes.grammarScreen,arguments: [ScreenEnum.third,FamilyString.textSubjectName]);
  }

  void _goToPractice() {
    Get.toNamed(AppRoutes.practiceScreen,arguments: ScreenEnum.third);
  }
}
