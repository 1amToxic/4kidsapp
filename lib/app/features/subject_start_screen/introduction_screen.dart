import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:for_kids_app/app/core/values/app_assets.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:for_kids_app/app/utils/enum_screen.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

class IntroductionScreen extends StatelessWidget {
  const IntroductionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GameScreen(
          title: Text(
            IntroductionString.textSubjectName,
            style: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(color: BaseDarkColor.colorSystemYellow),
          ),
          onBackPress: () => _goToBackScreen(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  GestureDetector(
                      onTap: () => _goToPronunciation(),
                      child: SvgPicture.asset(AppAssets.icPronunciation)),
                  const Expanded(
                    child: SizedBox(),
                  )
                ],
              ),
              Image.asset(AppImage.imageBridge),
              GestureDetector(onTap: () => _goToPractice() , child: SvgPicture.asset(AppAssets.icPractice)),
            ],
          )),
    );
  }

  void _goToBackScreen() {
    Get.offNamed(AppRoutes.homeScreen);
  }

  void _goToPronunciation() {
    Get.toNamed(AppRoutes.grammarScreen, arguments: [ScreenEnum.first,IntroductionString.textSubjectName ]);
  }

  void _goToPractice() {
    Get.toNamed(AppRoutes.practiceScreen,arguments: ScreenEnum.first);
  }
}
