import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/features/choose_word/viewmodel/choose_word_target_state.dart';
import 'package:for_kids_app/app/features/choose_word/viewmodel/choose_word_viewmodel.dart';
import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:for_kids_app/app/widgets/back_dialog.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../viewmodel/choose_word_state.dart';

class ChooseWordScreen extends StatelessWidget {
  late final ChooseWordViewModel _viewmodel;
  ChooseWordScreen({Key? key}) : super(key: key) {
    _viewmodel = Get.find<ChooseWordViewModel>();
  }

  @override
  Widget build(BuildContext context) {
    final param = Get.arguments;
    log("CWS ${param['screen']} ${param['subject']}");
    final horizontalPadding = Get.size.width / 10;
    return GameScreen(
      // title: "Ghép từ thích hợp vào ô dưới mỗi hình ảnh",
      title: FittedBox(
        child: Text("Ghép từ thích hợp vào ô dưới mỗi hình ảnh",
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: Colors.white),
            overflow: TextOverflow.ellipsis),
      ),
      showLaurel: true,
      scores: _viewmodel.scores,
      paddingContent:
          EdgeInsets.fromLTRB(horizontalPadding, 0, horizontalPadding, 10),
      onBackPress: onBackPress,
      onAskClick: onAskPress,
      onSettingsClick: onSettingPress,
      child: _chooseWordScreen(context),
    );
  }

  Widget _chooseWordScreen(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Obx(
          () {
            if (_viewmodel.images.isEmpty) return const SizedBox();
            return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for (int i = 0; i < 3; i++)
                    _imageWithDragTarget(
                        context: context, data: _viewmodel.images[i], index: i)
                ]);
          },
        ),
        // SizedBox(
        //   height: Get.height / 200,
        // ),
        Obx(
          () {
            if (_viewmodel.words.isEmpty) return const SizedBox();
            return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for (int i = 0; i < 6; i++)
                    _itemAnswer(_viewmodel.words[i], i)
                ]);
          },
        ),
        // SizedBox(
        //   height: Get.height/ 200,
        // ),
        Obx(
          () {
            if (_viewmodel.images.isEmpty) return const SizedBox();
            return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for (int i = 3; i < 6; i++)
                    _imageWithDragTarget(
                        context: context,
                        data: _viewmodel.images[i],
                        index: i,
                        reverse: true)
                ]);
          },
        ),
      ],
    );
  }

  Widget _imageWithDragTarget({
    required BuildContext context,
    required Rx<ChooseWordTargetState> data,
    required int index,
    bool reverse = false,
  }) {
    var image = SvgPicture.asset(
      data.value.content.imagePath,
      width: Get.width / 7,
      height: Get.height / 4,
    );

    String acceptData = data.value.answerAccept ?? "";
    bool accept = acceptData == "" ? false : true;
    bool isRightorWrongAnswer =
        data.value.answerState is RightAnswerAnswerState ||
            data.value.answerState is WrongAnswerAnswerState;
    var target = Obx(
      () => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          color: data.value.answerState?.color,
        ),
        width: image.width,
        height: image.height! / 3,
        child: DragTarget<String>(
          builder: (context, candidateData, rejectedData) {
            return accept
                ? FittedBox(
                    child: Center(
                      child: Text(
                        acceptData,
                        style: Get.textTheme.bodyMedium!.copyWith(
                          color: isRightorWrongAnswer
                              ? Colors.white
                              : BaseDarkColor.colorSystemGray3,
                        ),
                      ),
                    ),
                  )
                : Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                      color: candidateData.isEmpty
                          ? BaseDarkColor.colorSystemTeal
                          : GameColor.colorLabelBackground,
                    ),
                    width: image.width,
                    height: image.height! / 3,
                  );
          },
          onWillAccept: (data) {
            return !accept;
          },
          onAccept: (value) {
            accept = true;
            acceptData = value;
            _viewmodel.onAcceptWord(value, index);
          },
        ),
      ),
    );

    if (!reverse) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          image,
          const SizedBox(
            height: 5,
          ),
          target
        ],
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        target,
        const SizedBox(
          height: 5,
        ),
        image
      ],
    );
  }

  Widget _itemAnswer(Rx<ChooseWordState> word, int index) {
    return Obx(() => Draggable<String>(
          data: word.value.content,
          child: Text(
            word.value.content,
            style: Get.textTheme.bodyMedium!.copyWith(
              color: word.value.visiable
                  ? BaseLightColor.colorSystemOrange
                  : Colors.transparent,
            ),
          ),
          feedback: Text(
            word.value.content,
            style: Get.textTheme.bodyMedium!.copyWith(
              color: BaseLightColor.colorSystemOrange,
            ),
          ),
          childWhenDragging: Text(
            word.value.content,
            style: Get.textTheme.bodyMedium!.copyWith(
              color: Colors.transparent,
            ),
          ),
          onDragCompleted: () {
            _viewmodel.onDragComplete(index);
          },
        ));
  }

  onBackPress() {
    BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?"))
        .show();
  }

  onSettingPress() {}

  onAskPress() {}
}
