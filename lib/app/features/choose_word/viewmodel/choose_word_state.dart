class ChooseWordState {
  late final String content;
  late final bool visiable;

  ChooseWordState({required this.content, this.visiable = true});

  ChooseWordState copyWith({String? content, bool? visiable}) {
    return ChooseWordState(
      content: content ?? this.content,
      visiable: visiable ?? this.visiable,
    );
  }
}
