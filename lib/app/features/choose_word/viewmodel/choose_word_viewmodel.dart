import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/features/choose_word/viewmodel/choose_word_state.dart';
import 'package:for_kids_app/app/features/choose_word/viewmodel/choose_word_target_state.dart';
import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/app/utils/sound_effect.dart';
import 'package:for_kids_app/data/service/choose_word/choose_word_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../../core/values/app_strings.dart';
import '../../../utils/enum_screen.dart';

class ChooseWordViewModel extends BaseViewModel {
  final ChooseWordService service;
  final IUserRepository userRepository;
  final ISharedPreferenceApp sharedPreferenceApp;
  RxList<Rx<ChooseWordTargetState>> images =
      List<Rx<ChooseWordTargetState>>.empty(growable: true).obs;
  late RxList<Rx<ChooseWordState>> words =
      List<Rx<ChooseWordState>>.empty().obs;
  final checkAnswer = 0.obs;
  final scores = 0.obs;
  int answerWrongCount = 6;

  List<String> listSubject = List.empty();

  String subjectKey = "";

  int currentSubject = 0, sum = 0;

  String uid = "";

  ChooseWordViewModel(
      {required this.service,
      required this.sharedPreferenceApp,
      required this.userRepository}) {

    service.questions.listen((event) {
      words.clear();
      images.clear();
      answerWrongCount = 6;
      checkAnswer.value = 0;
      scores.value = 0;
      var tmp = event
          .map((e) => ChooseWordState(content: e.wordMeaning).obs)
          .toList();
      tmp.shuffle();
      words.value = tmp;
      images.value =
          event.map((e) => ChooseWordTargetState(content: e).obs).toList();
    });

    checkAnswer.listen((event) {
      if (event == answerWrongCount) {
        _onCheckAnswer();
      }
    });

	var argument = Get.arguments;
    ScreenEnum topic = argument["screen"];
    service.getDataByTopic(topic);

    currentSubject = argument["subject"];
    getDataFromSharePref(argument["screen"]);
  }

  void getDataFromSharePref(ScreenEnum screenEnum) async {
    subjectKey = "subject_${ScreenEnum.values.indexOf(screenEnum) + 1}";
    listSubject = await sharedPreferenceApp.getListString(subjectKey);
    uid = await sharedPreferenceApp.getString(SharePrefKey.uid);
    sum = await sharedPreferenceApp.getInt(SharePrefKey.sum);
  }

  Future<void> _onCheckAnswer() async {
    var tmp = images.value;
    for (int i = 0; i < 6; i++) {
      if (tmp[i].value.answerState is RightAnswerAnswerState) {
        continue;
      } else if (tmp[i].value.checkAnswer()) {
        scores.value += 10;
        tmp[i].value =
            tmp[i].value.copyWith(answerState: RightAnswerAnswerState());
        answerWrongCount--;
      } else {
        tmp[i].value =
            tmp[i].value.copyWith(answerState: WrongAnswerAnswerState());
      }
    }

    if (answerWrongCount == 0) {
      SoundEffect.playRightSound();
      finishQuestion();
    } else {
      SoundEffect.playWrongSound();
      await Future.delayed(
        const Duration(seconds: 2),
      );
      resetQuestion();
    }
  }

  finishQuestion() async {
    await Future.delayed(const Duration(seconds: 2));
    if (listSubject[currentSubject] == "0") {
      listSubject[currentSubject] = "1";
      sum += scores.value;
      sharedPreferenceApp.setListString(subjectKey, listSubject);
      sharedPreferenceApp.setInt(SharePrefKey.sum, sum);
      userRepository.updateFieldUser(subjectKey, listSubject, uid);
      await userRepository.updateFieldUser(SharePrefKey.sum, sum, uid);
    }
    Get.offNamed(AppRoutes.congratulationScreen, arguments: scores.value);
  }

  Future<void> onAcceptWord(String word, int index) async {
    var tmp = images[index].value;
    tmp = tmp.copyWith(answerAccept: word);
    images[index].value = tmp;
    checkAnswer.value++;
  }

  Future<void> resetQuestion() async {
    checkAnswer.value = 0;
    answerWrongCount = 0;
    for (int i = 0; i < 6; i++) {
      if (images[i].value.answerState is WrongAnswerAnswerState) {
        for (int j = 0; j < 6; j++) {
          if (words[j].value.content == images[i].value.answerAccept) {
            words[j].value = words[j].value.copyWith(visiable: true);
            images[i].value = images[i]
                .value
                .copyWith(answerAccept: "", answerState: InitialAnswerState());
            break;
          }
        }
        answerWrongCount++;
      }
    }
  }

  void onDragComplete(int index) {
    var tmp = words.value;
    tmp[index].value = tmp[index].value.copyWith(visiable: false);
    words.value = tmp;
  }
}
