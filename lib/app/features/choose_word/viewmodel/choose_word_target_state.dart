import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:for_kids_app/data/model/choose_word_model.dart';

class ChooseWordTargetState {
  late ChooseWordModel content;
  AnswerState? answerState;
  String? answerAccept;
  ChooseWordTargetState({required this.content}) {
    answerState = InitialAnswerState();
  }
  ChooseWordTargetState.withData({
    required this.content,
    required this.answerState,
    String? this.answerAccept,
  });

  ChooseWordTargetState copyWith(
      {ChooseWordModel? content,
      AnswerState? answerState,
      String? answerAccept}) {
    return ChooseWordTargetState.withData(
      content: content ?? this.content,
      answerState: answerState ?? this.answerState,
      answerAccept: answerAccept ?? this.answerAccept,
    );
  }

  bool checkAnswer() {
    if (answerAccept == null) return false;
    return content.wordMeaning == answerAccept!;
  }
}
