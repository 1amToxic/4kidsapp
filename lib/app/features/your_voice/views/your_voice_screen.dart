import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:for_kids_app/app/core/values/app_assets.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/features/your_voice/viewmodel/your_voice_viewmodel.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../../../widgets/back_dialog.dart';

class YourVoiceScreen extends GetView<YourVoiceViewModel> {
  YourVoiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: OnBoardingColor.colorAppBackgroundDark,
      body: GameScreen(
        showLaurel: true,
        scores: controller.scores,
        onAskClick: onAskClick,
        onSettingsClick: onSettingsClick,
        onBackPress: () => onBackPress(),
        child: _mainContent(),
      ),
    );
  }

  Widget _mainContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() => controller.yourVoiceState.value.iconPerson!),
            const SizedBox(
              width: 32,
            ),
            Obx(
              () => Container(
                padding: const EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  color: GameColor.colorLabelBackground,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(24),
                  ),
                  border: Border.all(
                    width: 5.0,
                    color: controller.yourVoiceState.value.color!,
                  ),
                ),
                child: Text(
                  controller.text.value,
                  style: Get.textTheme.headline3!.copyWith(color: Colors.black),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
        Obx(
          () => AvatarGlow(
            animate: controller.isListening.value,
            glowColor: controller.yourVoiceState.value.color!,
            duration: const Duration(milliseconds: 2000),
            repeatPauseDuration: const Duration(milliseconds: 100),
            repeat: true,
            endRadius: 60.0,
            child: GestureDetector(
              onTap: () => onTapMicro(),
              child: controller.yourVoiceState.value.iconMic,
            ),
          ),
        ),
      ],
    );
  }

  void onAskClick() {}

  void onSettingsClick() {}

  void onTapMicro() async {
    controller.onListening();
  }

  void onBackPress() async {
    BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?"))
        .show();
  }
}
