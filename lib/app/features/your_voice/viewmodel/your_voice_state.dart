part of "your_voice_viewmodel.dart";

class YourVoiceState {
  Color? color;
  SvgPicture? iconMic;
  SvgPicture? iconPerson;
  

  YourVoiceState();
  YourVoiceState.withData(
      {required this.color, required this.iconMic, required this.iconPerson});
}

class InitialYourVoiceState extends YourVoiceState {
  InitialYourVoiceState()
      : super.withData(
            color: BaseDarkColor.colorSystemYellow,
            iconMic: SvgPicture.asset(AppAssets.ic_audio_default),
            iconPerson: SvgPicture.asset(AppAssets.ic_ac_listen));
}

class RightAnswerYourVoiceState extends YourVoiceState {
  RightAnswerYourVoiceState()
      : super.withData(
            color: GameColor.colorRightAnswerBackground,
            iconMic: SvgPicture.asset(AppAssets.ic_audio_right),
            iconPerson: SvgPicture.asset(AppAssets.ic_ac_listen));
}

class WrongAnswerYourVoiceState extends YourVoiceState {
  WrongAnswerYourVoiceState()
      : super.withData(
            color: GameColor.colorWrongAnswerBackground,
            iconMic: SvgPicture.asset(AppAssets.ic_audio_wrong),
            iconPerson: SvgPicture.asset(AppAssets.ic_wa_listen));
}
