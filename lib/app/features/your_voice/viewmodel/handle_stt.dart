import 'package:flutter/material.dart';
import 'package:speech_to_text/speech_to_text.dart';

class HandleSpeechToText {
  static SpeechToText? _instance;

  static SpeechToText getInstance() {
    _instance ??= SpeechToText();
    return _instance!;
  }

  bool available = false;

  void Function(String status)? onStatus;
  void Function(dynamic error)? onError;

  void initialize() async {
    available = await _instance!.initialize(
      onStatus: onStatus,
	  onError: onError
    );
  }

  void onListen(Function(dynamic value) onResult) {
    _instance?.listen(onResult: (value) => onResult(value));
  }

  void stop() {
    _instance?.stop();
  }

  void cancel() {
    _instance?.cancel();
  }
}
