import 'dart:async';
import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_assets.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/utils/enum_screen.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/app/utils/sound_effect.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:for_kids_app/data/service/your_voice/your_voice_service.dart';
import 'package:get/get.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import '../../../core/values/app_strings.dart';
import '../../../routes/app_routes.dart';
part 'your_voice_state.dart';

class YourVoiceViewModel extends BaseViewModel {
  Rx<YourVoiceState> _yourVoiceState = YourVoiceState().obs;
  Rx<YourVoiceState> get yourVoiceState => _yourVoiceState;

  ISharedPreferenceApp sharedPreferenceApp;
  IUserRepository userRepository;

  static final _isListening = false.obs;
  get isListening => _isListening;

  bool _isSpeaking = false;

  final stt.SpeechToText _speechtt = stt.SpeechToText();

  final FlutterTts _textts = FlutterTts();

  final _text = "".obs;
  get text => _text;

  final _scores = 0.obs;
  RxInt get scores => _scores;

  String _speechText = "";

  String answer = "";

  List<String> listSubject = List.empty();

  String subjectKey = "";

  int currentSubject = 0, sum = 0;

  String uid = "";

  YourVoiceService service;

  bool _isScreenClose = false;

  Future<bool> avilable = Future.value(false);

  static VoidCallback? onCheckAnswer;

  YourVoiceViewModel(
      {required this.service,
      required this.sharedPreferenceApp,
      required this.userRepository}) {
    print("on create YourVoiceViewModel isListening: ${_isListening.hashCode}");
    var argument = Get.arguments;
    ScreenEnum topic = argument["screen"];
    service.getDataByTopic(topic);

    _yourVoiceState.value = InitialYourVoiceState();
    _textts.setLanguage('vi-VN');

    service.wordsStream.listen((event) async {
      _text.value = event as String;
      _isSpeaking = true;
      answer = event.replaceAll(RegExp(r'[,.?;:]'), '').toLowerCase();
      await _textts.speak(answer);
      _isSpeaking = false;
    });

    onCheckAnswer = checkAnswer;

    if (service.hasNextWord()) service.nextWord();
    currentSubject = argument["subject"];
    getDataFromSharePref(topic);
  }

  void getDataFromSharePref(ScreenEnum screenEnum) async {
    subjectKey = "subject_${ScreenEnum.values.indexOf(screenEnum) + 1}";
    listSubject = await sharedPreferenceApp.getListString(subjectKey);
    uid = await sharedPreferenceApp.getString(SharePrefKey.uid);
    sum = await sharedPreferenceApp.getInt(SharePrefKey.sum);
  }

  void onListening() async {
    print("isSpeaking: $_isSpeaking");
    if (!_isSpeaking && !_isListening.value) {
      avilable = _speechtt.initialize(
        onStatus: (value) {
          print("status: $value isListening: ${_isListening.hashCode}");
          //   if (!_isScreenClose) {
          if (value != "listening") {
            _isListening.value = false;
          }
          if (value == "done") {
            _isListening.value = false;
            onCheckAnswer?.call();
          }
          //   }
        },
        onError: (value) {
          _isListening.value = false;
          print("error: $value");
        },
      );
      if (await avilable) {
        _isListening.value = true;
        _speechtt.listen(
          onResult: (value) {
            _speechText = value.recognizedWords;
            print("speeach Text: $_speechText");
          },
        );
      } else {
        _isListening.value = false;
        _speechtt.stop();
      }
    } else {
      _isListening.value = false;
      _speechtt.stop();
    }
  }

  void checkAnswer() async {
    print("zzzzz " + answer + " // " + _speechText.toLowerCase());

    if (_speechText.toLowerCase() == answer) {
      _scores.value += 10;
      _yourVoiceState.value = RightAnswerYourVoiceState();
      // await if right answer
      SoundEffect.playRightSound();

      _nextWord();
    } else {
      _yourVoiceState.value = WrongAnswerYourVoiceState();
      SoundEffect.playWrongSound();
      await Future.delayed(const Duration(seconds: 2));
    }
  }

  void _nextWord() async {
    if (service.hasNextWord()) {
      await Future.delayed(const Duration(seconds: 2));
      _yourVoiceState.value = InitialYourVoiceState();
      service.nextWord();
    } else {
      if (listSubject[currentSubject] == "0") {
        listSubject[currentSubject] = "1";
        sum += scores.value;
        sharedPreferenceApp.setListString(subjectKey, listSubject);
        sharedPreferenceApp.setInt(SharePrefKey.sum, sum);
        userRepository.updateFieldUser(subjectKey, listSubject, uid);
        await userRepository.updateFieldUser(SharePrefKey.sum, sum, uid);
      }
      Get.offNamed(AppRoutes.congratulationScreen, arguments: scores.value);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void onClose() {
    super.onClose();
    log("OnDispose");
    _isScreenClose = true;
    _speechtt.stop();
    _textts.stop();
  }
}
