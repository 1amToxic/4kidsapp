import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/features/settings/viewmodels/settings_viewmodel.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../../../../core/values/app_colors.dart';

class PointScreen extends GetView<SettingsViewModel> {
  const PointScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GameScreen(
        showSetting: false,
        showAsk: false,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Text(
                PointString.title,
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: Colors.white),
              ),
            ),
            const Spacer(),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(
                    () => Text(
                      controller.sum.value.toString(),
                      style: const TextStyle(
                          fontSize: 36,
                          fontWeight: FontWeight.bold,
                          color: OnBoardingColor.colorButtonNav),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  SvgPicture.asset("assets/icons/ic_laurel.svg",width: 64,)
                ],
              ),
            ),
            const Spacer(
              flex: 2,
            )
          ],
        ),
      ),
    );
  }
}
