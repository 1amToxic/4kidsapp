import 'package:flutter/material.dart';
import 'package:for_kids_app/app/core/values/app_sizes.dart';
import 'package:for_kids_app/app/features/settings/viewmodels/edit_user_name_viewmodel.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

import '../../../../core/values/app_colors.dart';
import '../../../../core/values/app_strings.dart';

class EditUserName extends GetView<EditUserViewModel> {
  const EditUserName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: Get.height),
          child: GameScreen(
            child: _buildBody(context),
            showAsk: false,
            showSetting: false,
            onBackPress: () => _onBackPress(),
          ),
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          EditNameString.title,
          style: Theme.of(context)
              .textTheme
              .headline3!
              .copyWith(color: Colors.white),
        ),
        const ButtonUserName(
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildBorderButton(
                context,
                () => _onButtonCancel(),
                EditNameString.cancel,
                OnBoardingColor.colorButtonNav,
                Colors.transparent),
            const SizedBox(width: 48,),
            _buildBorderButton(
                context,
                () => _onButtonSave(),
                EditNameString.save,
                OnBoardingColor.colorAppBackgroundDark,
                OnBoardingColor.colorButtonNav),
          ],
        )
      ],
    );
  }

  SizedBox _buildBorderButton(BuildContext context, VoidCallback onButtonClick,
      String title, Color colorText, Color colorBgr) {
    return SizedBox(
      width: 110,
      child: OutlinedButton(
        onPressed: () => onButtonClick(),
        child: Text(
          title,
          style:
              Theme.of(context).textTheme.bodyText1!.copyWith(color: colorText),
        ),
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(colorBgr),
          side: MaterialStateProperty.all(
            BorderSide(
              color: BaseLightColor.colorSystemYellow,
              width: SettingsScreenSize.buttonBorderSideWidth,
              style: BorderStyle.solid,
            ),
          ),
        ),
      ),
    );
  }

  void _onButtonCancel() {
    Get.back();
  }

  void _onButtonSave() {
    controller.saveUserName(() => _showSuccess());
  }

  void _onBackPress() {
    Get.back();
  }


  void _showSuccess() {
    Get.snackbar("Thông báo", "Thay đổi tên đăng nhập thành công",colorText: Colors.white);
  }
}

class ButtonUserName extends GetView<EditUserViewModel> {
  const ButtonUserName({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 400,
      height: 64,
      child: Obx(
        () => TextFormField(
          key: controller.name.value == "" ? const ValueKey("") : const ValueKey("name"),
          initialValue: controller.name.value,
          onChanged: (value) {
            controller.updateUsername(value);
          },
          style: Theme.of(context)
              .textTheme
              .headline3!
              .copyWith(color: OnBoardingColor.colorAppBackgroundDark),
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(24)),
                borderSide: BorderSide(
                    width: 2, color: BaseDarkColor.colorSystemYellow),
              ),
              focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(24)),
                borderSide: BorderSide(
                    width: 5, color: BaseDarkColor.colorSystemYellow),
              ),
              hintStyle: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(color: OnBoardingColor.colorAppBackgroundDark)),
          cursorColor: BaseDarkColor.colorSystemIndigo,
          textAlign: TextAlign.start,
        ),
      ),
    );
  }
}
