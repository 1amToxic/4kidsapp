import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/features/settings/viewmodels/settings_viewmodel.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

class RewardScreen extends GetView<SettingsViewModel> {
  const RewardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GameScreen(
        showSetting: false,
        showAsk: false,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Text(
                RewardString.title,
                style: Theme
                    .of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: Colors.white),
              ),
            ),
            const Spacer(),
            Obx(
              (() =>
                  Expanded(
                    child: controller.rewardStatus.value != RewardStatus.none
                        ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        controller.rewardStatus.value ==
                            RewardStatus.hardWork
                            ? SvgPicture.asset(
                            "assets/icons/ic_hard_work.svg")
                            : SvgPicture.asset(
                            "assets/icons/ic_master.svg"),
                        const SizedBox(
                          width: 30,
                        ),
                        Text(
                          controller.rewardStatus.value ==
                              RewardStatus.hardWork
                              ? RewardString.hardWork
                              : RewardString.master,
                          style: Theme
                              .of(context)
                              .textTheme
                              .subtitle1!
                              .copyWith(
                              color: OnBoardingColor.colorButtonNav),
                        )
                      ],
                    )
                        : const SizedBox(),
                  )),
            ),
            const Spacer(
              flex: 2,
            )
          ],
        ),
      ),
    );
  }
}
