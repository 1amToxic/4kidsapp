import 'package:flutter/material.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/core/values/app_sizes.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/features/settings/viewmodels/settings_viewmodel.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

class SettingsScreen extends GetView<SettingsViewModel> {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GameScreen(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildBorderButton(context, () => _onButtonUsernameClick(),
                SettingsString.textButton1),
            _buildBorderButton(context, () => _onButtonRewardClick(),
                SettingsString.textButton2),
            _buildBorderButton(context, () => _onButtonPointClick(),
                SettingsString.textButton3),
            _buildBorderButton(context, () => _onButtonLogOutClick(),
                SettingsString.textButton4),
          ],
        ),
        showAsk: false,
        showSetting: false,
      ),
    );
  }

  SizedBox _buildBorderButton(
      BuildContext context, VoidCallback onButtonClick, String title) {
    return SizedBox(
      width: Get.height / 2,
      child: OutlinedButton(
        onPressed: () => onButtonClick(),
        child: Text(
          title,
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: SettingsColor.colorTextButton),
        ),
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: SettingsScreenSize.buttonBorderRadius,
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
          side: MaterialStateProperty.all(
            BorderSide(
              color: BaseLightColor.colorSystemYellow,
              width: SettingsScreenSize.buttonBorderSideWidth,
              style: BorderStyle.solid,
            ),
          ),
        ),
      ),
    );
  }

  void _onButtonUsernameClick() {
    Get.toNamed(AppRoutes.editUserNameScreen);
  }

  void _onButtonRewardClick() {
    Get.toNamed(AppRoutes.rewardScreen);
  }

  void _onButtonPointClick() {
    Get.toNamed(AppRoutes.pointScreen);
  }

  void _onButtonLogOutClick() {
    Get.snackbar("Thông báo", "Tính năng đang trong quá trình phát triển",colorText: Colors.white);
  }
}
