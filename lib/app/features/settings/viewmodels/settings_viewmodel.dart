import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/data/model/user/user_model.dart';
import 'package:for_kids_app/data/service/base/data_result.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

class SettingsViewModel extends BaseViewModel {
  final ISharedPreferenceApp _sharedPreferenceApp;
  final IUserRepository _userRepository;
  final user = User.withInitial("").obs;
  final name = "".obs;
  final sum = 0.obs;
  String uid = "";
  final rewardStatus = RewardStatus.none.obs;

  SettingsViewModel(this._sharedPreferenceApp, this._userRepository);

  void updateUsername(String name) {
    user.value.name = name;
    user.refresh();
  }

  void saveUserName(VoidCallback onSuccess) async {
    DataResult result =
        await _userRepository.updateUserToFirebase(user.value, uid);
    if (result.isSuccess) {
      _sharedPreferenceApp.setString(SharePrefKey.userName, user.value.name);
      onSuccess();
      log("SVM23: Change username success");
    } else {}
  }

  void logOut() {
    _sharedPreferenceApp.setString(SharePrefKey.userName, "");
  }

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    uid = await _sharedPreferenceApp.getString(SharePrefKey.uid);
    String name = await _sharedPreferenceApp.getString(SharePrefKey.userName);
    user.value.name = name;
    user.refresh();
    update();
    int point = await _sharedPreferenceApp.getInt(SharePrefKey.sum);
    sum.value = point;
    log("SVM53: ${user.value.toString()}");
    // point = user.value.sum;
    if (point >= 200) {
      log("SVM60: MASTER");
      rewardStatus.value = RewardStatus.master;
    } else {
      log("SVM63: HW");
      rewardStatus.value = RewardStatus.hardWork;
    }
    // }else{
    //   log("SVM59: ${(dataResult.error as APIFailure).errorResponse}");
    // }
  }
}

enum RewardStatus { hardWork, master,none }
