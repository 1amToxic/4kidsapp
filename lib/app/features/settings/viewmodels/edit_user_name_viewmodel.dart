import 'package:flutter/material.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:get/get.dart';

import '../../../../data/service/base/data_result.dart';
import '../../../../data/service/user/user_repository.dart';
import '../../../core/values/app_strings.dart';
import '../../../utils/shared_pref.dart';

class EditUserViewModel extends BaseViewModel {
  final ISharedPreferenceApp _sharedPreferenceApp;
  final IUserRepository _userRepository;

  EditUserViewModel(this._sharedPreferenceApp, this._userRepository);

  final name = "".obs;
  String uid = "";

  void updateUsername(String n) {
    name.value = n;
  }

  void saveUserName(VoidCallback onSuccess) async {
    DataResult result =
        await _userRepository.updateFieldUser("name", name.value, uid);
    if (result.isSuccess) {
      _sharedPreferenceApp.setString(SharePrefKey.userName, name.value);
      onSuccess();
    } else {}
  }

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    uid = await _sharedPreferenceApp.getString(SharePrefKey.uid);
    name.value = await _sharedPreferenceApp.getString(SharePrefKey.userName);
  }
}
