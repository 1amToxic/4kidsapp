import 'package:flutter/material.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/features/find_missing_word/viewmodel/find_missing_word_viewmodel.dart';
import 'package:for_kids_app/app/widgets/back_dialog.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import 'package:get/get.dart';

class FindMissingWordScreen extends GetWidget<FindMissingWordViewModel> {
  late FindMissingWordViewModel _viewModel;
  late Size size;
  FindMissingWordScreen() {
    _viewModel = Get.find<FindMissingWordViewModel>();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: OnBoardingColor.colorAppBackgroundDark,
      body: GameScreen(
        title: FittedBox(
          child: Text(
            "Chọn từ thích hợp vào chỗ trống",
            style: Get.textTheme.headline3!.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
			overflow: TextOverflow.ellipsis
          ),
        ),
        paddingContent:
            EdgeInsets.fromLTRB(size.width / 10, 5, size.width / 10, 10),
        showLaurel: true,
        scores: _viewModel.scores,
        onAskClick: onAskClick,
        onSettingsClick: onSettingClick,
        onBackPress: onBackPress,
        child: _findMissingWordGame(context),
      ),
    );
  }

  Widget _findMissingWordGame(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Obx(
          () => Text(
            _viewModel.question.value,
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: BaseDarkColor.colorSystemYellow),
          ),
        ),
        SizedBox(
          height: size.height / 20
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _answer(context: context, index: 0),
            const SizedBox(
              width: 10,
            ),
            _answer(context: context, index: 2)
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _answer(context: context, index: 1),
            const SizedBox(
              width: 10,
            ),
            _answer(context: context, index: 3)
          ],
        ),
      ],
    );
  }

  Widget _answer({required BuildContext context, required int index}) {
    return Obx(
      () => GestureDetector(
        onTap: () => _viewModel.onAnswer(index: index),
        child: Container(
          width: size.width / 3,
          decoration: BoxDecoration(
            color: _viewModel.answerStates[index].value.color,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
            padding: EdgeInsets.all(size.width / 60),
            child: Center(
              child: FittedBox(
                child: Text(
                  _viewModel.answers[index].value,
                  style: Theme.of(context).textTheme.bodyText1!,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  onAskClick() {}
  onSettingClick() {}
  onBackPress() {
    BackDialog(content: const Text("Bạn có muốn thoát khỏi màn hình không?"))
        .show();
  }
}
