part of 'find_missing_word_viewmodel.dart';

class AnswerState {
  Color? color;
  AnswerState();
  AnswerState.withData({required this.color});
}

class InitialAnswerState extends AnswerState {
  InitialAnswerState() : super.withData(color: GameColor.colorLabelBackground);
}

class RightAnswerAnswerState extends AnswerState {
  RightAnswerAnswerState()
      : super.withData(color: GameColor.colorRightAnswerBackground);
}

class WrongAnswerAnswerState extends AnswerState {
  WrongAnswerAnswerState()
      : super.withData(color: GameColor.colorWrongAnswerBackground);
}
