import 'dart:async';

import 'package:flutter/material.dart';
import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_colors.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:for_kids_app/app/utils/sound_effect.dart';
import 'package:for_kids_app/data/service/find_missing_word/find_missing_word_service.dart';
import 'package:for_kids_app/data/service/user/user_repository.dart';
import 'package:get/get.dart';

import '../../../../data/model/find_missing_word_model.dart';
import '../../../routes/app_routes.dart';
import '../../../utils/enum_screen.dart';
part 'answer_state.dart';

class FindMissingWordViewModel extends BaseViewModel {
  FindMissingWordService service;
  ISharedPreferenceApp sharedPreferenceApp;
  IUserRepository userRepository;
  final scores = 0.obs;
  bool isSelect = false;
  final List<FindMissingWordModel> listWrongAnswer = List.empty(growable: true);
  late FindMissingWordModel currentQuestion;
  final question = "".obs;
  final answers = ["".obs, "".obs, "".obs, "".obs];

  List<String> listSubject = List.empty(growable: true);

  String subjectKey = "";

  int currentSubject = 0, sum = 0;

  String uid = "";

  final answerStates = [
    AnswerState().obs,
    AnswerState().obs,
    AnswerState().obs,
    AnswerState().obs
  ];

  FindMissingWordViewModel(
      {required this.service,
      required this.sharedPreferenceApp,
      required this.userRepository}) {
    var argument = Get.arguments;
    ScreenEnum topic = argument["screen"];
    service.getDataByTopic(topic);
    service.currentQuestion.listen((value) {
      currentQuestion = value;
      question.value = currentQuestion.question;
      for (int i = 0; i < 4; i++) {
        answers[i].value = currentQuestion.answers[i];
        answerStates[i].value = InitialAnswerState();
      }
    });
    if (service.hasNextQuestion()) service.nextQuestion();
    currentSubject = argument["subject"];
    getDataFromSharePref(topic);
  }

  void getDataFromSharePref(ScreenEnum screenEnum) async {
    subjectKey = "subject_${ScreenEnum.values.indexOf(screenEnum) + 1}";
    listSubject = await sharedPreferenceApp.getListString(subjectKey);
    uid = await sharedPreferenceApp.getString(SharePrefKey.uid);
    sum = await sharedPreferenceApp.getInt(SharePrefKey.sum);
  }

  onAnswer({required int index}) async {
    if (!isSelect) {
      isSelect = true;
      if (currentQuestion.rightAnswerIndex == index) {
        answerStates[index].value = RightAnswerAnswerState();
        scores.value += 10;
        SoundEffect.playRightSound();
      } else {
        answerStates[index].value = WrongAnswerAnswerState();
        listWrongAnswer.add(FindMissingWordModel.copyWith(currentQuestion));
        SoundEffect.playWrongSound();
      }
      _nextQuestion();
    }
  }

  _nextQuestion() async {
    if (service.hasNextQuestion()) {
      isSelect = false;
      await Future.delayed(const Duration(seconds: 2));
      service.nextQuestion();
    } else if (listWrongAnswer.isNotEmpty) {
      print(listWrongAnswer);
      isSelect = false;
      await Future.delayed(const Duration(seconds: 1));
      service.setData(listWrongAnswer);
      listWrongAnswer.clear();
    } else {
      if (listSubject[currentSubject] == "0") {
        listSubject[currentSubject] = "1";
        sum += scores.value;
        sharedPreferenceApp.setListString(subjectKey, listSubject);
        sharedPreferenceApp.setInt(SharePrefKey.sum, sum);
        userRepository.updateFieldUser(subjectKey, listSubject, uid);
        await userRepository.updateFieldUser(SharePrefKey.sum, sum, uid);
      }
      Get.offNamed(AppRoutes.congratulationScreen, arguments: scores.value);
    }
  }
}
