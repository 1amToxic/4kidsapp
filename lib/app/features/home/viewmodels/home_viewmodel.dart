import 'dart:developer';

import 'package:for_kids_app/app/base/base_viewmodel.dart';
import 'package:for_kids_app/app/core/values/app_strings.dart';
import 'package:for_kids_app/app/utils/shared_pref.dart';
import 'package:get/get.dart';

class HomeViewModel extends BaseViewModel {
  final ISharedPreferenceApp _sharedPreferenceApp;
  final point1 = 0.obs;
  final point2 = 0.obs;
  final point3 = 0.obs;
  final point4 = 0.obs;
  final point5 = 0.obs;
  int subjectOpen = 1;
  HomeViewModel(this._sharedPreferenceApp);

  void getAllPoint() async {
    point1.value =
        (await _sharedPreferenceApp.getListString(SharePrefKey.subject1))
            .where((element) => element == "1")
            .length;
    point2.value =
        (await _sharedPreferenceApp.getListString(SharePrefKey.subject2))
            .where((element) => element == "1")
            .length;
    point3.value =
        (await _sharedPreferenceApp.getListString(SharePrefKey.subject3))
            .where((element) => element == "1")
            .length;
    point4.value =
        (await _sharedPreferenceApp.getListString(SharePrefKey.subject4))
            .where((element) => element == "1")
            .length;
    point5.value =
        (await _sharedPreferenceApp.getListString(SharePrefKey.subject5))
            .where((element) => element == "1")
            .length;
    if(point1.value == 3) subjectOpen = 2;
    if(point2.value == 3) subjectOpen = 3;
    if(point3.value == 3) subjectOpen = 4;
    if(point4.value == 3) subjectOpen = 5;
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    log("HVM INIT");

  }
}
