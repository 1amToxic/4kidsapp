import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SubjectWidget extends StatelessWidget {
  const SubjectWidget({
    Key? key,
    required this.isLock,
    required this.star,
    required this.isRectangleRotate,
    required this.onTap,
    required this.iconPath,
  }) : super(key: key);
  final bool isLock;
  final int star;
  final bool isRectangleRotate;
  final VoidCallback onTap;
  final String iconPath;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: isLock
          ? Stack(
              children: [
                SvgPicture.asset(
                  iconPath,
                  height: 110,
                  width: 110,
                ),
                isLock
                    ? (isRectangleRotate
                        ? const KeySquareRotate()
                        : const KeyShape())
                    : StarGroup(itemCount: star)
              ],
            )
          : Column(
              children: [
                SvgPicture.asset(
                  iconPath,
                  height: 110,
                  width: 110,
                ),
                StarGroup(itemCount: star)
              ],
            ),
    );
  }
}

class KeySquareRotate extends StatelessWidget {
  const KeySquareRotate({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 20,
      bottom: 0,
      child: SvgPicture.asset(
        "assets/icons/ic_lock.svg",
        width: 32,
        height: 32,
      ),
    );
  }
}

class KeyShape extends StatelessWidget {
  const KeyShape({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 10,
      bottom: 0,
      child: SvgPicture.asset(
        "assets/icons/ic_lock.svg",
        width: 32,
        height: 32,
      ),
    );
  }
}

class StarGroup extends StatelessWidget {
  const StarGroup({
    Key? key,
    required this.itemCount,
  }) : super(key: key);
  final int itemCount;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 20,
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 2.0),
            child: SvgPicture.asset(
              "assets/icons/ic_star.svg",
              height: 20,
              width: 20,
            ),
          );
        },
        itemCount: itemCount,
      ),
    );
  }
}
