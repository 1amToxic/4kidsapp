import 'package:flutter/material.dart';
import 'package:for_kids_app/app/widgets/game_screen.dart';
import '../viewmodels/home_viewmodel.dart';
import 'package:for_kids_app/app/routes/app_routes.dart';
import 'package:get/get.dart';

import 'widgets/subject_widget.dart';

class HomeScreen extends GetView<HomeViewModel> {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    controller.getAllPoint();
    return Scaffold(
      body: GameScreen(
        child: _buildBody(),
        showBack: false,
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Obx(
              () => SubjectWidget(
                isLock: false,
                isRectangleRotate: false,
                star: controller.point1.value,
                onTap: () => _goToIntroduction(),
                iconPath: "assets/icons/ic_subject_1.svg",
              ),
            ),
            Obx(
              () => SubjectWidget(
                isLock: controller.subjectOpen < 2,
                isRectangleRotate: true,
                star: controller.point2.value,
                onTap: () => _goToSchool(),
                iconPath: "assets/icons/ic_subject_2.svg",
              ),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Obx(
              () => SubjectWidget(
                isLock: controller.subjectOpen < 3,
                isRectangleRotate: false,
                star: controller.point3.value,
                onTap: () => _goToFamily(),
                iconPath: "assets/icons/ic_subject_3.svg",
              ),
            ),
            Obx(
              () => SubjectWidget(
                isLock: controller.subjectOpen < 4,
                isRectangleRotate: false,
                star: controller.point4.value,
                onTap: () => _goToSky(),
                iconPath: "assets/icons/ic_subject_4.svg",
              ),
            ),
            Obx(
              () => SubjectWidget(
                isLock: controller.subjectOpen < 5,
                isRectangleRotate: false,
                star: controller.point5.value,
                onTap: () => _goToJob(),
                iconPath: "assets/icons/ic_subject_5.svg",
              ),
            ),
          ],
        ),
        const Spacer()
      ],
    );
  }

  void _goToIntroduction() {
    Get.toNamed(AppRoutes.introductionScreen);
  }

  void _goToSchool() {
    if (controller.subjectOpen < 2) {
      _showDialogCannotAccess();
    } else {
      Get.toNamed(AppRoutes.schoolScreen);
    }
  }

  void _goToFamily() {
    if (controller.subjectOpen < 3) {
      _showDialogCannotAccess();
    } else {
      Get.toNamed(AppRoutes.familyScreen);
    }
  }

  void _goToSky() {
    if (controller.subjectOpen < 4) {
      _showDialogCannotAccess();
    } else {
      Get.toNamed(AppRoutes.skyScreen);
    }
  }

  void _goToJob() {
    if (controller.subjectOpen < 5) {
      _showDialogCannotAccess();
    } else {
      Get.toNamed(AppRoutes.jobScreen);
    }
  }

  void _showDialogCannotAccess() {
    Get.snackbar("Thông báo",
        "Bé phải hoàn thành các chủ đề trước để mở khóa chủ đề này!",
        colorText: Colors.white);
  }
}
