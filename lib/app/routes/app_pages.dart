import 'package:for_kids_app/app/bindings/edit_user_name_binding.dart';
import 'package:for_kids_app/app/bindings/point_screen_binding.dart';
import 'package:for_kids_app/app/bindings/reward_binding.dart';
import 'package:for_kids_app/app/features/settings/views/widgets/edit_user_name.dart';
import 'package:for_kids_app/app/features/settings/views/widgets/logout_screen.dart';
import 'package:for_kids_app/app/features/settings/views/widgets/point_screen.dart';
import 'package:for_kids_app/app/features/settings/views/widgets/reward_screen.dart';
import 'package:for_kids_app/app/widgets/congratulation_screen.dart';

import '../bindings/choose_word_binding.dart';
import '../base/practice_binding.dart';
import '../bindings/home_bindings.dart';
import '../bindings/onboard_bindings.dart';
import '../bindings/settings_binding.dart';
import '../bindings/splash_bindings.dart';
import '../features/choose_word/view/choose_word_screen.dart';
import '../features/home/views/home_screen.dart';
import '../binding/find_missing_word_binding.dart';
import '../bindings/family_album_binding.dart';
import '../features/family_album/views/family_album_screen.dart';
import '../binding/sort_word_binding.dart';
import '../features/find_missing_word/views/find_missing_word_screen.dart';
import '../bindings/flipthepuzzle_binding.dart';
import '../features/flip_the_puzzle/views/flipthepuzzle_screen.dart';
import '../bindings/grammar_binding.dart';
import '../features/grammar/views/grammar_game_screen.dart';
import '../features/onboard/views/onboard_screen.dart';
import '../features/practice/views/practice_screen.dart';
import '../features/settings/views/settings_screen.dart';
import '../features/splash/views/splash_screen.dart';
import '../binding/your_voice_binding.dart';
import '../features/sort_word/views/sort_word_screen.dart';
import '../features/subject_start_screen/family_screen.dart';
import '../features/subject_start_screen/introduction_screen.dart';
import '../features/subject_start_screen/school_screen.dart';
import '../features/subject_start_screen/sky_screen.dart';
import '../features/your_voice/views/your_voice_screen.dart';
import 'app_routes.dart';
import 'package:get/get.dart';

import '../features/subject_start_screen/job_screen.dart';

class AppPages {
  static const initial = AppRoutes.flipThePuzzleScreen;
  static final routes = [
    GetPage(
        name: AppRoutes.initialScreen,
        page: () => OnBoardingScreen(),
        binding: OnBoardBinding()),
    GetPage(
      name: AppRoutes.yourVoiceScreen,
      page: () => YourVoiceScreen(),
      binding: YourVoiceBinding(),
    ),
    GetPage(
      name: AppRoutes.findMissingWordScreen,
      page: () => FindMissingWordScreen(),
      binding: FindMissingWordBinding(),
    ),
    GetPage(
      name: AppRoutes.sortWordScreen,
      page: () => SortWordScreen(),
      binding: SortWordBinding(),
    ),
    GetPage(
      name: AppRoutes.homeScreen,
      page: () => const HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
        name: AppRoutes.settingsScreen,
        page: () => const SettingsScreen(),
        binding: SettingsBinding()),
    GetPage(
      name: AppRoutes.flipThePuzzleScreen,
      page: () => const FlipThePuzzleScreen(),
      binding: FlipThePuzzleBinding(),
    ),
    GetPage(
      name: AppRoutes.familyAlbumScreen,
      page: () => const FamilyAlbumScreen(),
      binding: FamilyAlbumBinding(),
    ),
    GetPage(
      name: AppRoutes.grammarScreen,
      page: () => const GrammarGameScreen(),
      binding: GrammarBinding(),
    ),
    GetPage(
      name: AppRoutes.splashScreen,
      page: () => const SplashScreen(),
      binding: SplashBindings(),
    ),
    GetPage(
      name: AppRoutes.chooseWordScreen,
      page: () => ChooseWordScreen(),
      binding: ChooseWordBinding(),
    ),
    GetPage(
      name: AppRoutes.introductionScreen,
      page: () => const IntroductionScreen(),
    ),
    GetPage(
      name: AppRoutes.schoolScreen,
      page: () => const SchoolScreen(),
    ),
    GetPage(
      name: AppRoutes.familyScreen,
      page: () => const FamilyScreen(),
    ),
    GetPage(
      name: AppRoutes.skyScreen,
      page: () => const SkyScreen(),
    ),
    GetPage(
      name: AppRoutes.jobScreen,
      page: () => const JobScreen(),
    ),
    GetPage(
        name: AppRoutes.practiceScreen,
        page: () => PracticeScreen(),
        binding: PracticeBinding()),
    GetPage(
        name: AppRoutes.editUserNameScreen,
        page: () => const EditUserName(),
        binding: EditUserNameBinding()),
    GetPage(
        name: AppRoutes.rewardScreen,
        page: () => const RewardScreen(),
        binding: RewardBinding()),
    GetPage(
        name: AppRoutes.pointScreen,
        page: () => const PointScreen(),
        binding: PointBinding()),
    GetPage(name: AppRoutes.logoutScreen, page: () => const LogOutScreen()),
    GetPage(
        name: AppRoutes.congratulationScreen,
        page: () => const Congratulation()),
    GetPage(
      name: AppRoutes.rewardScreen,
      page: () => const RewardScreen(),
      binding: RewardBinding(),
    ),
    GetPage(
      name: AppRoutes.pointScreen,
      page: () => const PointScreen(),
      binding: PointBinding(),
    ),
    GetPage(
      name: AppRoutes.logoutScreen,
      page: () => const LogOutScreen(),
    ),
    GetPage(
      name: AppRoutes.congratulationScreen,
      page: () => Congratulation(),
    ),
  ];
}
