class AppRoutes {
  static const String initialScreen = "/onboard";
  static const String homeScreen = "/home";
  static const String settingsScreen = "/settings";
  static const String splashScreen = "/splash";
  static const String yourVoiceScreen = "/yourVoice";
  static const String findMissingWordScreen = "/findMissingWord";
  static const String sortWordScreen = "/sortWorScreen";
  static const String flipThePuzzleScreen = "/flip_the_puzzle";
  static const String chooseWordScreen = "/chooseWordScreen";
  static const String familyAlbumScreen = '/familyAlbumScreen';
  static const String grammarScreen = "/grammar";
  static const String introductionScreen = "/introduction";
  static const String schoolScreen = "/school";
  static const String familyScreen = "/family";
  static const String skyScreen = "/sky";
  static const String jobScreen = "/job";
  static const String practiceScreen = "/practice";
  static const String editUserNameScreen = "/edit_user_name";
  static const String rewardScreen = "/reward";
  static const String pointScreen = "/point";
  static const String logoutScreen = "/logout";
  static const String congratulationScreen = "/congratulationScreen";


}
