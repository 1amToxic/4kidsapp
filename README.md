# for_kids_app

## Getting Started

## Libs
- Getx: https://pub.dev/packages/get/install
- flip_card: https://pub.dev/packages/flip_card/install
- equatable: https://pub.dev/packages/equatable/install
- flutter_tts: https://pub.dev/packages/flutter_tts/install


## Font:
- UTM Azuki

## Package Structure: (Updating ...)
- app:
    - base: base of application
    - core: strings, theme, or constants of application
    - routes: routing of application
    - widgets: common widgets in application
- data:
    - base: base data of data layer
- main